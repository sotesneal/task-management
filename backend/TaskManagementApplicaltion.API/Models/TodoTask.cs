﻿using System.ComponentModel.DataAnnotations;
using TaskManagementApplication.API.Enums;


namespace TaskManagementApplication.API.Models
{
    public class TodoTask
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public Priority Priority { get; set; }
        public Status Status { get; set; }
        public string CreatorUserId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsArchived { get; set; }
        public bool IsPinned { get; set; }
        public virtual ApplicationUser CreatorUser { get; set; }
        public virtual ICollection<TaskAuditLog> AuditLogs { get; set; }
    }
}
