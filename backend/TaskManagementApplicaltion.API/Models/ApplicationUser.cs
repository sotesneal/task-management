﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace TaskManagementApplication.API.Models
{
    public class ApplicationUser : IdentityUser
    {


        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ProfilePicturePath { get; set; } = String.Empty;

        public virtual ICollection<TodoTask> TodoTasks { get; set; }
        public virtual ICollection<TaskAuditLog> AuditLogs { get; set; }
    }
}
