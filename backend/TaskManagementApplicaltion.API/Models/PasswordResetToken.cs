﻿using System.ComponentModel.DataAnnotations;

namespace TaskManagementApplication.API.Models
{
    public class PasswordResetToken
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        [Required]
        public bool IsUsed { get; set; }
    }
}
