﻿using System.ComponentModel.DataAnnotations;
using TaskManagementApplication.API.Enums;

namespace TaskManagementApplication.API.Models
{
    public class TaskAuditLog
    {
        [Key]
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public AuditAction Action { get; set; }

        public int TaskId { get; set; } // Foreign key for Task
        public virtual TodoTask Task { get; set; }

        public string CreatorUserId{ get; set; } // Foreign key for User
        public virtual ApplicationUser User { get; set; }

        public string OldValues { get; set; }
        public string NewValues { get; set; }
    }

}
