﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskManagementApplication.API.Migrations
{
    /// <inheritdoc />
    public partial class addisarchived : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsArchived",
                table: "TodoTasks",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsArchived",
                table: "TodoTasks");
        }
    }
}
