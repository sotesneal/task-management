﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using TaskManagementApplication.API.DTOS.AuditLog;
using TaskManagementApplication.API.Enums;
using TaskManagementApplication.API.Exceptions;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories;
using TaskManagementApplication.API.Services.Interfaces;

namespace TaskManagementApplication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator")]
    public class AuditLogsController : ControllerBase
    {
        private readonly IAuditLogService _auditLogService;
        private readonly ILogger<AuditLogsController> _logger;

        public AuditLogsController(IAuditLogService auditLogService, ILogger<AuditLogsController> logger)
        {
            _auditLogService = auditLogService;
            _logger = logger;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadAuditLogDto>>> GetAuditLogs([FromQuery] string? action = null, [FromQuery] DateTime? fromTimestamp = null, [FromQuery] DateTime? toTimestamp = null, [FromQuery] string? sortBy = null, [FromQuery] bool includeUsername = false)
        {
            try
            {
                var filterOptions = new AuditLogFilterOptions
                {
                    Action = !string.IsNullOrEmpty(action) ? Enum.TryParse(action, ignoreCase: true, out AuditAction auditAction) ? auditAction : null : null,
                    FromTimestamp = fromTimestamp,
                    ToTimestamp = toTimestamp,
                    Username = null!
                };

                var auditLogs = await _auditLogService.GetAuditLogsAsync(filterOptions, sortBy!, includeUsername);
                return Ok(auditLogs);
            }
            catch (RepositoryException ex)
            {
                _logger.LogError(ex, "An error occurred while fetching audit logs");
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

    }
}
