﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SixLabors.ImageSharp.Formats.Jpeg;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using TaskManagementApplication.API.DataAccess;
using TaskManagementApplication.API.DTOS.Auth;
using TaskManagementApplication.API.DTOS.Email;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Services;
using TaskManagementApplication.API.Utlilities;
using static System.Net.Mime.MediaTypeNames;

namespace TaskManagementApplication.API.Controllers
{
    [ApiController]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AccountController> _logger;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly EmailService _emailService;
        private readonly ApplicationDbContext _dbContext;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IConfiguration configuration, ILogger<AccountController> logger, IWebHostEnvironment hostingEnvironment, EmailService emailService, ApplicationDbContext dbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _emailService = emailService;
            _dbContext = dbContext;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterDto model)
        {
            _logger.LogInformation("Registering a new user...");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Check if the username or email already exists in the database
            var existingUser = await _userManager.FindByNameAsync(model.UserName);
            if (existingUser != null)
            {
                ModelState.AddModelError("username", "Username already exists. Please use a different username.");
            }

            existingUser = await _userManager.FindByEmailAsync(model.Email);
            if (existingUser != null)
            {
                ModelState.AddModelError("email", "Email already exists. Please use a different email.");
            }

            if (!ModelState.IsValid)
            {
                // If there are validation errors, return them in the expected format
                var errors = new Dictionary<string, string[]>();

                foreach (var key in ModelState.Keys)
                {
                    var errorMessages = ModelState[key]!.Errors.Select(e => e.ErrorMessage).ToArray();
                    if (errorMessages.Length > 0)
                    {
                        errors[key] = errorMessages;
                    }
                }

                _logger.LogWarning("User registration failed: {Errors}", errors);
                return BadRequest(errors);
            }

            var user = new ApplicationUser
            {
                UserName = model.UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email
            };

            try
            {
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation("User registered successfully.");

                    // Assign roles to the user during registration
                    await _userManager.AddToRoleAsync(user, "User");

                    return Ok();
                }
                else
                {
                    // If the registration fails, return the errors from the backend as a response
                    var registrationErrors = result.Errors.Select(error => error.Description).ToList();
                    _logger.LogWarning("User registration failed: {Errors}", registrationErrors);
                    return BadRequest(registrationErrors);
                }
            }
            catch (Exception ex)
            {
                // Log the exception and return a generic error message
                _logger.LogError(ex, "An error occurred while registering a user.");
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDto model)
        {
            _logger.LogInformation("User login attempt...");

            // Attempt to find the user by email
            var user = await _userManager.FindByEmailAsync(model.EmailOrUserName);

            // If the user is not found by email, attempt to find by username
            if (user == null)
            {
                user = await _userManager.FindByNameAsync(model.EmailOrUserName);
            }

            if (user == null)
            {
                _logger.LogWarning("User not found.");
                return StatusCode(404, new { ErrorCode = "UserNotFound" });
            }

            try
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    _logger.LogInformation("User login successful.");
           

                    var token = await GenerateJwtToken(user); // Generate token only if login succeeds
                    return StatusCode(200, new { Token = token });
                }
                else
                {
                    // If the login fails, return a custom error message
                    _logger.LogWarning("User login failed: Invalid password.");
                    return Unauthorized(new { ErrorCode = "InvalidEmailOrPassword" });
                }
            }
            catch (Exception ex)
            {
                // Log the exception and return a generic error message
                _logger.LogError(ex, "An error occurred while logging in.");
                return StatusCode(500, new { ErrorCode = "ServerError" });
            }
        }



        private async Task<string> GenerateJwtToken(ApplicationUser user)
        {
            _logger.LogInformation("Generating JWT token for user {UserId}...", user.Id);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Aud, _configuration["Jwt:Audience"]!),
                new Claim(JwtRegisteredClaimNames.Iss, _configuration["Jwt:Issuer"]!),
            };

            // Add user roles as claims
            var roles = await _userManager.GetRolesAsync(user);
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            // Add profile picture URL as a claim
            if (!string.IsNullOrEmpty(user.ProfilePicturePath))
            {
                claims.Add(new Claim("ProfilePictureUrl", user.ProfilePicturePath));
            }

            // Create token descriptor
            var key = Encoding.ASCII.GetBytes(_configuration["Jwt:SecretKey"]!);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(3), // Token expiration time
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            // Generate and return the token
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            _logger.LogInformation("JWT token generated successfully: {Token}", tokenString);
            return tokenString;
        }

        [HttpPost("upload-profile-picture")]
        public async Task<IActionResult> UploadProfilePicture(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return BadRequest("No file uploaded.");
            }

            var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            var profilePicturesPath = Path.Combine(_hostingEnvironment.WebRootPath, "profile-pictures");
            var filePath = Path.Combine(profilePicturesPath, fileName);

            if (!Directory.Exists(profilePicturesPath))
            {
                Directory.CreateDirectory(profilePicturesPath);
            }

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                var relativePath = "profile-pictures/" + fileName; // Relative path from wwwroot
                user.ProfilePicturePath = relativePath;
                await _userManager.UpdateAsync(user);
            }
            else
            {
                return BadRequest("User not found.");
            }

            var publicUrl = Url.Content("~/profile-pictures/" + fileName); // Public URL accessible from frontend

            return Ok(new { ProfilePictureUrl = publicUrl });
        }
        private async Task ProcessAndSaveImage(IFormFile file, Stream outputStream)
        {
            try
            {
                // Load the uploaded image using SixLabors.ImageSharp
                using (var image = SixLabors.ImageSharp.Image.Load(file.OpenReadStream()))
                {
                    // Implement image processing here, e.g., compression, resizing, format conversion, etc.
                    // Example: Resize the image to a maximum width of 800 pixels
                    image.Mutate(x => x.Resize(new ResizeOptions
                    {
                        Size = new Size(800, 0),
                        Mode = ResizeMode.Max
                    }));

                    // Save the processed image to the output stream
                    await image.SaveAsync(outputStream, new JpegEncoder());
                }
            }
            catch (Exception ex)
            {
                // Handle any exceptions that occur during image processing
                // You can log the error or return an appropriate response to the client
                throw new Exception("Error processing image: " + ex.Message);
            }
        }

    

        [HttpGet("userinfo")]
        public IActionResult GetUserInfo()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = _userManager.FindByIdAsync(userId).Result;

            if (user == null)
            {
                return NotFound();
            }

            var userInfo = new { Username = user.UserName, Firstname = user.FirstName, Lastname = user.LastName, Email = user.Email, ProfilePictureUrl = user.ProfilePicturePath};
            return Ok(userInfo);
        }

        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordDto model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Email))
            {
                return BadRequest("Email is required for password reset.");
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            _logger.LogInformation("User found: {User}", user);
            if (user == null)
            {
                return Ok("If an account with this email exists, a password reset email will be sent.");
            }

            try
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                // Generate the callback URL with port 3000
                var callbackUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.Host}:3000/reset-password/{user.Id}/{WebUtility.UrlEncode(token)}";

                // Save the token in the PasswordResetTokens table
                var passwordResetToken = new PasswordResetToken
                {
                    UserId = user.Id,
                    Token = token,
                    ExpirationDate = DateTime.UtcNow.AddHours(1), // Set the expiration time (adjust as needed)
                    IsUsed = false 
                };
                _dbContext.PasswordResetTokens.Add(passwordResetToken);
                await _dbContext.SaveChangesAsync();

                _logger.LogInformation("Generated token: {Token}", token);
                _logger.LogInformation("Callback URL: {CallbackUrl}", callbackUrl);

                // Send the password reset email using the EmailService
                await _emailService.SendPasswordResetEmailAsync(model.Email, callbackUrl!);

                return Ok("If an account with this email exists, a password reset email will be sent.");
            }
            catch (Exception ex)
            {
                // Log the exception and return a generic error message
                _logger.LogError(ex, "An error occurred while initiating password reset.");
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }



        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordDto model)
        {
            // Find the corresponding user
            var user = await _userManager.FindByIdAsync(model.UserId); // Use userId instead of email

            if (user == null)
            {
                // Handle the case where the user does not exist (return an error response)
                return BadRequest("User not found.");
            }
            var newToken = await _userManager.GeneratePasswordResetTokenAsync(user);

            // Find the corresponding token in the database
            var userToken = await _dbContext.PasswordResetTokens
                .Where(t => t.UserId == user.Id && t.Token == model.Token && !t.IsUsed && t.ExpirationDate >= DateTime.UtcNow)
                .FirstOrDefaultAsync();

            if (userToken == null)
            {
                // Invalid or expired token, or already used, handle accordingly (e.g., show an error message).
                return BadRequest("Invalid or expired token.");
            }

            // Reset the user's password
            var resetPasswordResult = await _userManager.ResetPasswordAsync(user, newToken, model.NewPassword);

            _logger.LogInformation("Password reset result: {Result}", resetPasswordResult);

            if (resetPasswordResult.Succeeded)
            {
                // Mark the token as used
                userToken.IsUsed = true;
                await _dbContext.SaveChangesAsync();

                return Ok("Password reset successful.");
            }
            else
            {
                var resetErrors = resetPasswordResult.Errors.Select(error => error.Description).ToList();
                _logger.LogError("An error occurred while resetting the password: {Errors}", resetErrors);
                return BadRequest(resetErrors);
            }
        }
        [HttpPost("change-password")]
        [Authorize] 
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDto model)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);    // Get the current user

                if (user == null)
                {
                    return BadRequest("User not found.");
                }

                // Verify the old password
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

                if (result.Succeeded)
                {
                    return Ok("Password changed successfully.");
                }
                else
                {
                    var changePasswordErrors = result.Errors.Select(error => error.Description).ToList();
                    _logger.LogWarning("Password change failed: {Errors}", changePasswordErrors);
                    return BadRequest(changePasswordErrors);
                }
            }
            catch (Exception ex)
            {
                // Log the exception and return a generic error message
                _logger.LogError(ex, "An error occurred while changing the password.");
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }
        [HttpPut("update-email")]
        [Authorize]
        public async Task<IActionResult> UpdateEmail(UpdateEmailDto model)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);

                if (user == null)
                {
                    return BadRequest("User not found.");
                }

                // Ensure that the new email is not already used by another user
                var existingUserWithEmail = await _userManager.FindByEmailAsync(model.Email);
                if (existingUserWithEmail != null && existingUserWithEmail.Id != user.Id)
                {
                    return BadRequest("Email already exists. Please use a different email.");
                }

                user.Email = model.Email;
                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    return Ok("Email updated successfully.");
                }
                else
                {
                    var updateErrors = result.Errors.Select(error => error.Description).ToList();
                    _logger.LogWarning("Email update failed: {Errors}", updateErrors);
                    return BadRequest(updateErrors);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while updating the Email.");
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }
        [HttpPut("update-lastname")]
        [Authorize]
        public async Task<IActionResult> UpdateLastname(UpdateLastnameDto model)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);

                if (user == null)
                {
                    return BadRequest("User not found.");
                }

                user.LastName = model.LastName;
                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    return Ok("Lastname updated successfully.");
                }
                else
                {
                    var updateErrors = result.Errors.Select(error => error.Description).ToList();
                    _logger.LogWarning("Lastname update failed: {Errors}", updateErrors);
                    return BadRequest(updateErrors);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while updating the Lastname.");
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }
        [HttpPut("update-firstname")]
        [Authorize]
        public async Task<IActionResult> UpdateFirstname(UpdateFirstnameDto model)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User); // Get the current user

                if (user == null)
                {
                    return BadRequest("User not found.");
                }

                user.FirstName = model.FirstName; // Update the FirstName property
                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    return Ok("Firstname updated successfully.");
                }
                else
                {
                    var updateErrors = result.Errors.Select(error => error.Description).ToList();
                    _logger.LogWarning("Firstname update failed: {Errors}", updateErrors);
                    return BadRequest(updateErrors);
                }
            }
            catch (Exception ex)
            {
                // Log the exception and return a generic error message
                _logger.LogError(ex, "An error occurred while updating the Firstname.");
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }


    }
}
