﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TaskManagementApplication.API.Services;

namespace TaskManagementApplication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactUsController : ControllerBase
    {
        private readonly EmailService _emailService;

        public ContactUsController(EmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost("send")]
        public async Task<IActionResult> SendContactUsEmailAsync([FromBody] ContactUsRequest request)
        {
            try
            {
                // Validate the request data (e.g., name, email, message)
                if (string.IsNullOrWhiteSpace(request.Name) ||
                    string.IsNullOrWhiteSpace(request.Email) ||
                    string.IsNullOrWhiteSpace(request.Message))
                {
                    return BadRequest("Name, Email, and Message are required fields.");
                }

                // Send the contact us email
                await _emailService.SendContactUsEmailAsync(request.Email, "Contact Us Request", request.Message);

                // Optionally, you can log the successful email sending here.

                return Ok("Contact us email sent successfully!");
            }
            catch (Exception ex)
            {
                // Log the error
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while sending the contact us email.");
            }
        }
    }

    public class ContactUsRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
    }
}
