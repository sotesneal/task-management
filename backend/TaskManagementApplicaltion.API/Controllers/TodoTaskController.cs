﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Claims;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;
using TaskManagementApplication.API.DTOS.TodoTask;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories;
using TaskManagementApplication.API.Repositories.Interfaces;
using TaskManagementApplication.API.Services.Exceptions;
using TaskManagementApplication.API.Services.Interfaces;

namespace TaskManagementApplication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "User")]
    public class TodoTasksController : ControllerBase
    {
        private readonly ITodoTaskService _todoTaskService;
        private readonly ILogger<TodoTasksController> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;

        public TodoTasksController(ITodoTaskService todoTaskService, ILogger<TodoTasksController> logger, IHttpContextAccessor httpContextAccessor, UserManager<ApplicationUser> userManager)
        {
            _todoTaskService = todoTaskService;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadTodoTaskDto>>> GetAllTasks([FromQuery] TaskFilterOptions filterOptions = null!,[FromQuery] string? sortBy = null)
        {
            _logger.LogInformation("Fetching all tasks.");

            try
            {
                // Get the current user's ID
                var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                if (userId == null)
                {
                    return Unauthorized("User is not authenticated.");
                }
                _logger.LogInformation($"Fetching tasks for user with ID: {userId}");

                var tasks = await _todoTaskService.GetAllTodoTask(userId, filterOptions, sortBy);
                return Ok(tasks);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while fetching tasks.");
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReadTodoTaskDto>> GetTaskById(int id)
        {
            _logger.LogInformation("Fetching task with ID {TaskId}.", id);

            try
            {
                var task = await _todoTaskService.GetTodoTaskById(id);
                return Ok(task);
            }
            catch (NotFoundException ex)
            {
                _logger.LogWarning(ex, "Task with ID {TaskId} not found.", id);
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while fetching task with ID {TaskId}.", id);
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateTask(CreateTodoTaskDto createTodoTaskDto)
        {
            _logger.LogInformation("Creating a new task.");

            try
            {
                var userId = _httpContextAccessor.HttpContext!.User.FindFirstValue(ClaimTypes.NameIdentifier);


                string message = $"User {userId} is creating a blog post";
                _logger.LogInformation(message);

                var createdTask = await _todoTaskService.CreateTodoTask(createTodoTaskDto,userId);
                return CreatedAtAction(nameof(GetTaskById), new { id = createdTask.Id }, createdTask);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while creating a new task.");
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ReadTodoTaskDto>> UpdateTask(int id, UpdateTodoTaskDto updateTodoTaskDto)
        {
            _logger.LogInformation("Updating task with ID {TaskId}.", id);

            try
            {
                if (id != updateTodoTaskDto.Id)
                {
                    _logger.LogWarning("Request URL ID ({UrlId}) does not match payload ID ({PayloadId}).", id, updateTodoTaskDto.Id);
                    return BadRequest("ID mismatch between request URL and payload.");
                }

                var updatedTask = await _todoTaskService.UpdateTodoTask(updateTodoTaskDto);
                return Ok(updatedTask);
            }
            catch (NotFoundException ex)
            {
                _logger.LogWarning(ex, "Task with ID {TaskId} not found.", id);
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while updating task with ID {TaskId}.", id);
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTask(int id)
        {
            _logger.LogInformation("Deleting task with ID {TaskId}.", id);

            try
            {
                var isDeleted = await _todoTaskService.DeleteTodoTask(id);
                if (isDeleted)
                {
                    return NoContent();
                }
                else
                {
                    _logger.LogWarning("Task with ID {TaskId} not found.", id);
                    return NotFound("Task not found.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while deleting task with ID {TaskId}.", id);
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        [HttpPut("archive/{id}")]
        public async Task<IActionResult> ArchiveTask(int id)
        {
            try
            {
                var archivedTask = await _todoTaskService.ArchiveTaskAsync(id);
                return Ok(archivedTask);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                // Log the exception and return an appropriate error response
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }
        [HttpGet("archive")]
        public async Task<ActionResult<IEnumerable<ReadTodoTaskDto>>> GetAllArchivedTasks()
        {
            _logger.LogInformation("Fetching all archived tasks.");

            try
            {
                // Get the current user's ID
                var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                if (userId == null)
                {
                    return Unauthorized("User is not authenticated.");
                }
                _logger.LogInformation($"Fetching archived tasks for user with ID: {userId}");

                var tasks = await _todoTaskService.GetAllArchivedTasks(userId);
                return Ok(tasks);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while fetching archived tasks.");
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }
        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<ReadTodoTaskDto>>> SearchTasks([FromQuery] string searchTerm)
        {
            try
            {
                // Get the current user's ID
                var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                if (userId == null)
                {
                    return Unauthorized("User is not authenticated.");
                }

                _logger.LogInformation($"Searching tasks for user with ID: {userId} using search term: {searchTerm}");

                // Call your service or repository to perform the task search
                var searchResults = await _todoTaskService.SearchTasks(userId, searchTerm);

                return Ok(searchResults);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while searching tasks.");
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }
        [HttpPut("{taskId}/pin")]
        public async Task<IActionResult> UpdateTaskPinnedStatus(int taskId, [FromBody] bool isPinned)
        {
            try
            {
                var updated = await _todoTaskService.UpdateTaskPinnedStatusAsync(taskId, isPinned);

                if (!updated)
                {
                    return NotFound(); // Task not found
                }

                return Ok(); // Task pinned status updated successfully
            }
            catch (Exception ex)
            {
                // Handle exceptions and return an appropriate error response
                return StatusCode(500, "Internal server error");
            }
        }
        

        [HttpPost("unarchive/{taskId}")]
        public async Task<IActionResult> UnarchiveTask(int taskId)
        {
            try
            {
                var unarchivedTask = await _todoTaskService.UnarchiveTaskAsync(taskId);

                if (unarchivedTask == null)
                {
                    return NotFound("Task not found");
                }

                var options = new JsonSerializerOptions
                {
                    ReferenceHandler = ReferenceHandler.Preserve, // Handle circular references
                    WriteIndented = true, // For pretty-printing the JSON output (optional)
                };

                var json = JsonSerializer.Serialize(unarchivedTask, options);

                return Ok(json); // Return the unarchived task as JSON
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                // Log the exception
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("archive/search")]
        public async Task<ActionResult<IEnumerable<ReadTodoTaskDto>>> SearchArchivedTasks([FromQuery] string searchTerm)
        {
            try
            {
                // Get the current user's ID
                var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                if (userId == null)
                {
                    return Unauthorized("User is not authenticated.");
                }

                _logger.LogInformation($"Searching archived tasks for user with ID: {userId} using search term: {searchTerm}");

                // Call your service or repository to perform the archived task search
                var searchResults = await _todoTaskService.SearchArchivedTasks(userId, searchTerm);

                return Ok(searchResults);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while searching archived tasks.");
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }


    }
}
