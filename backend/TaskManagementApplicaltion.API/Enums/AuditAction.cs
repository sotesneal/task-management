﻿using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;

namespace TaskManagementApplication.API.Enums
{
    public enum AuditAction
    {
        [EnumMember(Value = "Create")]
        Create,
        [EnumMember(Value = "Update")]
        Update,
        [EnumMember(Value = "Delete")]
        Delete,
        [EnumMember(Value = "SoftDelete")]
        SoftDelete
    }

}
