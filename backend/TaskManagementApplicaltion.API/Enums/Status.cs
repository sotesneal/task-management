﻿namespace TaskManagementApplication.API.Enums
{
    public enum Status
    {
        None,
        ToDo,
        InProgress,
        Done
    }
}
