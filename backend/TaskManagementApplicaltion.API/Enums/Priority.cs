﻿namespace TaskManagementApplication.API.Enums
{
    public enum Priority
    {
        Low,
        Medium,
        High
    }
}
