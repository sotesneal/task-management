﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TaskManagementApplication.API.Models;

namespace TaskManagementApplication.API.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<TodoTask> TodoTasks { get; set; }
        public DbSet<TaskAuditLog> TaskAuditLogs { get; set; }
        public DbSet<PasswordResetToken> PasswordResetTokens { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<TaskAuditLog>()
                .HasOne(a => a.User)
                .WithMany()
                .HasForeignKey(a => a.CreatorUserId);

            modelBuilder.Entity<TaskAuditLog>()
                .HasOne(a => a.Task)
                .WithMany(t => t.AuditLogs)
                .HasForeignKey(a => a.TaskId);

            modelBuilder.Entity<TodoTask>()
                .HasOne(t => t.CreatorUser)
                .WithMany(u => u.TodoTasks)
                .HasForeignKey(t => t.CreatorUserId);

            // Avoid cascading delete on TaskAuditLogs
            modelBuilder.Entity<TodoTask>()
                .HasMany(t => t.AuditLogs)
                .WithOne(a => a.Task)
                .HasForeignKey(a => a.TaskId)
                .OnDelete(DeleteBehavior.Restrict);

            // Soft delete filter for TodoTasks
            modelBuilder.Entity<TodoTask>()
                .HasQueryFilter(t => !t.IsDeleted);
        }
    }
}
