﻿namespace TaskManagementApplication.API
{
    public enum SortDirection
    {
        Ascending,
        Descending
    }
}