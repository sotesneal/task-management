﻿using TaskManagementApplication.API.Enums;

namespace TaskManagementApplication.API.Repositories
{
    public class AuditLogFilterOptions
    {
        public string Username { get; set; }
        public AuditAction? Action { get; set; }
        public Status? Status { get; set; }
        public DateTime? FromTimestamp { get; set; }
        public DateTime? ToTimestamp { get; set; }
    }
}
