﻿using System.ComponentModel;

namespace TaskManagementApplication.API.Repositories
{
    public class TaskSortOptions
    {
        public TaskSortField SortField { get; set; }
        public SortDirection SortDirection { get; set; }
    }
}
