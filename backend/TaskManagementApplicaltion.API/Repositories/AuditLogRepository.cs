﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using TaskManagementApplication.API.DataAccess;
using TaskManagementApplication.API.Enums;
using TaskManagementApplication.API.Exceptions;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories.Interfaces;

namespace TaskManagementApplication.API.Repositories
{
    public class AuditLogRepository : IAuditLogRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<AuditLogRepository> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        public AuditLogRepository(ApplicationDbContext context, ILogger<AuditLogRepository> logger, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _logger = logger;
            _userManager = userManager;

        }

        public async Task<IEnumerable<TaskAuditLog>> GetAuditLogsAsync( AuditLogFilterOptions filterOptions = null!, string? sortBy = null, bool includeUsername = false)
        {
            try
            {
                IQueryable<TaskAuditLog> query = _context.TaskAuditLogs;


                if (filterOptions != null)
                {
                    if (!string.IsNullOrEmpty(filterOptions.Username))
                    {
                        query = query.Where(log => log.CreatorUserId == filterOptions.Username);
                    }

                    if (filterOptions.Action.HasValue)
                    {
                        query = query.Where(log => log.Action == filterOptions.Action.Value);
                    }

                    if (filterOptions.FromTimestamp.HasValue)
                    {
                        query = query.Where(log => log.Timestamp >= filterOptions.FromTimestamp.Value);
                    }

                    if (filterOptions.ToTimestamp.HasValue)
                    {
                        query = query.Where(log => log.Timestamp <= filterOptions.ToTimestamp.Value);
                    }
                }

                // Apply sorting based on sortBy parameter
                if (!string.IsNullOrEmpty(sortBy))
                {
                    if (sortBy.Equals("timestamp", StringComparison.OrdinalIgnoreCase))
                    {
                        query = query.OrderBy(log => log.Timestamp);
                    }
                    // Add more sorting criteria if needed
                }

                var auditLogs = await query.ToListAsync();

                if (includeUsername)
                {
                    // Fetch usernames using userIds and update auditLogs
                    var userIds = auditLogs.Select(log => log.CreatorUserId).Distinct().ToList();
                    var userIdToUsername = await _userManager.Users
                        .Where(user => userIds.Contains(user.Id))
                        .ToDictionaryAsync(user => user.Id, user => user.UserName);

                    foreach (var log in auditLogs)
                    {
                        if (userIdToUsername.TryGetValue(log.CreatorUserId, out var username))
                        {
                            log.CreatorUserId = username;
                        }
                    }
                }
                foreach (var log in auditLogs)
                {
                    log.NewValues = HideNullOriginal(log.NewValues);
                    log.OldValues = HideNullOriginal(log.OldValues);
                }

                _logger.LogInformation("Retrieved {AuditLogCount} audit logs", auditLogs.Count);

                return auditLogs;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while fetching audit logs");
                throw new RepositoryException("Error occurred while fetching audit logs.", ex);
            }
        }
        private string HideNullOriginal(string json)
        {
            if (json == null)
            {
                return null;
            }

            try
            {
                using (JsonDocument document = JsonDocument.Parse(json))
                {
                    if (document.RootElement.ValueKind == JsonValueKind.Object)
                    {
                        var newJson = new Dictionary<string, JsonElement>();
                        foreach (var property in document.RootElement.EnumerateObject())
                        {
                            if (property.Value.ValueKind != JsonValueKind.Null)
                            {
                                newJson[property.Name] = property.Value;
                            }
                        }
                        return JsonSerializer.Serialize(newJson);
                    }
                }
            }
            catch (JsonException ex)
            {
                _logger.LogError(ex, "Error occurred while parsing or manipulating JSON");
            }

            return json; // Return the original JSON if any errors occur
        }


    }
}
