﻿using TaskManagementApplication.API.Models;

namespace TaskManagementApplication.API.Repositories.Interfaces
{
    public interface ITodoTaskRepository
    {
        Task<TodoTask> GetTaskByIdAsync(int taskId);
        Task<IEnumerable<TodoTask>> GetAllTasksAsync(string userId, TaskFilterOptions filterOptions = null!, string? sortBy = null);
        Task <TodoTask> CreateTaskAsync(TodoTask task);
        Task <TodoTask> UpdateTaskAsync(TodoTask task, bool unarchive = false);
        Task<bool> DeleteTaskAsync(int taskId);
        Task ArchiveAsync(TodoTask task);
        Task<IEnumerable<TodoTask>> GetAllArchivedTasksAsync(string userId);
        Task<IEnumerable<TodoTask>> SearchTasksAsync(string userId, string searchTerm);
        Task<IEnumerable<TodoTask>> SearchArchivedTasksAsync(string userId, string searchTerm);
        Task<bool> UpdateTaskPinnedStatusAsync(int taskId, bool isPinned);
    }
}
