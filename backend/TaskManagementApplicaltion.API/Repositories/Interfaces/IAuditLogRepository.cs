﻿using TaskManagementApplication.API.Models;

namespace TaskManagementApplication.API.Repositories.Interfaces
{
    public interface IAuditLogRepository
    {
        Task<IEnumerable<TaskAuditLog>> GetAuditLogsAsync(AuditLogFilterOptions filterOptions = null!, string? sortBy = null, bool includeUsername = false);
    }
}
