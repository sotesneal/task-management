﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using TaskManagementApplication.API.DataAccess;
using TaskManagementApplication.API.Enums;
using TaskManagementApplication.API.Exceptions;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories.Interfaces;

namespace TaskManagementApplication.API.Repositories
{
    public class TodoTaskRepository : ITodoTaskRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<TodoTaskRepository> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TodoTaskRepository(ApplicationDbContext context, ILogger<TodoTaskRepository> logger, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task ArchiveAsync(TodoTask task)
        {
            if (task == null) 
            {
                throw new ArgumentNullException(nameof(task));
            }
            task.IsArchived = true;
            await _context.SaveChangesAsync();
        }

        public async Task<TodoTask> CreateTaskAsync(TodoTask task)
        {
            try
            {
                if (task == null)
                {
                    throw new ArgumentNullException(nameof(task));
                }

                _context.TodoTasks.Add(task);
                await _context.SaveChangesAsync();

                // Create audit log for task creation
                await CreateAuditLogAsync(AuditAction.Create, null!, task);

                _logger.LogInformation("Task created: {TaskId}", task.Id);
                return task;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while creating task");

                // Log the exception
                // Throw a custom exception or return an appropriate error response
                throw new RepositoryException("Error occurred while creating task.", ex);
            }
        }



        public async Task<bool> DeleteTaskAsync(int taskId)
        {
            try
            {
                var task = await _context.TodoTasks.FindAsync(taskId);
                if (task == null)
                {
                    return false; // Task with the specified ID doesn't exist
                }

                // Set the IsDeleted flag to true
                task.IsDeleted = true;

                // Update the task entity
                _context.Entry(task).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                // Create audit log for task deletion
                await CreateAuditLogAsync(AuditAction.SoftDelete, task, null!);

                _logger.LogInformation("Task deleted: {TaskId}", taskId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting task");

                // Log the exception
                // Throw a custom exception or return an appropriate error response
                throw new RepositoryException("Error occurred while deleting task.", ex);
            }
        }

        public async Task<IEnumerable<TodoTask>> GetAllArchivedTasksAsync(string userId)
        {
            try
            {
                IQueryable<TodoTask> query = _context.TodoTasks.Where(task => task.CreatorUserId == userId);

                var archivedTasks = await query.Where(task => task.IsArchived == true).ToListAsync();
                return archivedTasks;
            }
            catch (Exception ex) 
            {
                throw new RepositoryException("Error occurred while fetching tasks.", ex);
            }
        }

        public async Task<IEnumerable<TodoTask>> GetAllTasksAsync(string userId, TaskFilterOptions filterOptions, string? sortBy = null)
        {
            try
            {
                IQueryable<TodoTask> query = _context.TodoTasks.Where(task => task.CreatorUserId == userId);

                // Apply filtering based on filter options
                if (filterOptions != null)
                {
                    if (filterOptions.Status.HasValue)
                    {
                        query = query.Where(task => task.Status == filterOptions.Status.Value);
                    }

                    if (filterOptions.DueDate.HasValue)
                    {
                        query = query.Where(task => task.DueDate == filterOptions.DueDate.Value);
                    }
                }

                // Apply sorting based on sortBy parameter
                if (!string.IsNullOrEmpty(sortBy))
                {
                    if (sortBy.Equals("dueDate", StringComparison.OrdinalIgnoreCase))
                    {
                        query = query.OrderBy(task => task.DueDate);
                    }
                    else if (sortBy.Equals("priority", StringComparison.OrdinalIgnoreCase))
                    {
                        query = query.OrderBy(task => task.Priority);
                    }
                    // Add more sorting criteria if needed
                }

                var tasks = await query.ToListAsync();

                _logger.LogInformation("Retrieved {TaskCount} tasks for user with ID {UserId}", tasks.Count, userId);

                return tasks;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while fetching tasks");

                // Log the exception
                // Throw a custom exception or return an appropriate error response
                throw new RepositoryException("Error occurred while fetching tasks.", ex);
            }
        }




        public async Task<TodoTask> GetTaskByIdAsync(int taskId)
        {
            try
            {
                var task = await _context.TodoTasks.FindAsync(taskId);

                if (task == null)
                {
                    _logger.LogInformation("Task not found: {TaskId}", taskId);
                }
                else
                {
                    _logger.LogInformation("Retrieved task: {TaskId}", taskId);
                }

                return task!;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while fetching task by ID");

                // Log the exception
                // Throw a custom exception or return an appropriate error response
                throw new RepositoryException("Error occurred while fetching task by ID.", ex);
            }
        }

        public async Task<TodoTask> UpdateTaskAsync(TodoTask task, bool unarchive = false)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();

            try
            {
                if (task == null)
                {
                    throw new ArgumentNullException(nameof(task));
                }

                // If unarchive is true, only update the IsArchived property
                if (unarchive)
                {
                    var existingTask = await _context.TodoTasks
                        .Where(t => t.Id == task.Id)
                        .FirstOrDefaultAsync();

                    if (existingTask == null)
                    {
                        throw new Exception("Task not found");
                    }

                    existingTask.IsArchived = false;
                    _context.Entry(existingTask).State = EntityState.Modified;
                }
                else
                {
                    // Update the entire task object
                    _context.Entry(task).State = EntityState.Modified;
                }

                await _context.SaveChangesAsync();
                await transaction.CommitAsync();

                // Create audit log for task update (if needed)
                await CreateAuditLogAsync(AuditAction.Update, task, task);

                _logger.LogInformation("Task updated: {TaskId}", task.Id);

                return task;
            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();

                _logger.LogError(ex, "Error occurred while updating task");

                // Log the exception
                // Throw a custom exception or return an appropriate error response
                throw new RepositoryException("Error occurred while updating task.", ex);
            }
        }

        private async Task CreateAuditLogAsync(AuditAction action, TodoTask oldTask, TodoTask newTask)
        {
            var userId = GetCurrentUserId();
            var auditLog = new TaskAuditLog
            {
                Timestamp = DateTime.UtcNow,
                Action = action,
                CreatorUserId = userId,
                TaskId = oldTask != null ? oldTask.Id : (newTask != null ? newTask.Id : 0),
                OldValues = oldTask != null ? JsonConvert.SerializeObject(oldTask) : "None",
                NewValues = newTask != null ? JsonConvert.SerializeObject(newTask) : "None"
            };

            _context.TaskAuditLogs.Add(auditLog);
            await _context.SaveChangesAsync();
        }

        private string GetCurrentUserId()
        {
            var userClaim = _httpContextAccessor.HttpContext!.User.FindFirst(ClaimTypes.NameIdentifier);
            return userClaim!.Value;
        }
        public async Task<IEnumerable<TodoTask>> SearchTasksAsync(string userId, string searchTerm)
        {
            try
            {
                // Convert the search term to lowercase
                string searchTermLower = searchTerm?.ToLower() ?? string.Empty;

                IQueryable<TodoTask> query = _context.TodoTasks
                    .Where(task => task.CreatorUserId == userId &&
                        task.IsDeleted != true && task.IsArchived != true && // Exclude deleted and archived tasks
                        (task.Title.ToLower().Contains(searchTermLower) || task.Description.ToLower().Contains(searchTermLower)));

                var searchResults = await query.ToListAsync();

                return searchResults;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while searching tasks");

                // Log the exception
                // Throw a custom exception or return an appropriate error response
                throw new RepositoryException("Error occurred while searching tasks.", ex);
            }
        }
        public async Task<IEnumerable<TodoTask>> SearchArchivedTasksAsync(string userId, string searchTerm)
        {
            try
            {
                // Convert the search term to lowercase
                string searchTermLower = searchTerm?.ToLower() ?? string.Empty;

                IQueryable<TodoTask> query = _context.TodoTasks
                    .Where(task => task.CreatorUserId == userId &&
                        task.IsArchived == true && task.IsDeleted != true &&
                        (task.Title.ToLower().Contains(searchTermLower) || task.Description.ToLower().Contains(searchTermLower)));

                // Include archived tasks in the search results
                var searchResults = await query.ToListAsync();

                return searchResults;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while searching archived tasks");

                // Log the exception
                // Throw a custom exception or return an appropriate error response
                throw new RepositoryException("Error occurred while searching archived tasks.", ex);
            }
        }

        public async Task<bool> UpdateTaskPinnedStatusAsync(int taskId, bool isPinned)
        {
            try
            {
                var task = await _context.TodoTasks.FindAsync(taskId);

                if (task == null)
                {
                    return false; // Task not found
                }

                task.IsPinned = isPinned;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                // Handle exceptions
                throw new RepositoryException("Error updating task pinned status.", ex);
            }
        }
    }
}
