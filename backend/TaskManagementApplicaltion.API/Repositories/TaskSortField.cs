﻿namespace TaskManagementApplication.API
{
    public enum TaskSortField
    {
        DueDate,
        Priority,
    }
}