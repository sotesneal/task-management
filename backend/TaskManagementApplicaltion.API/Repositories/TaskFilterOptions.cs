﻿using TaskManagementApplication.API.Enums;

namespace TaskManagementApplication.API.Repositories
{
    public class TaskFilterOptions
    {
        public Status? Status { get; set; }
        public DateTime? DueDate { get; set; }
    }
}
