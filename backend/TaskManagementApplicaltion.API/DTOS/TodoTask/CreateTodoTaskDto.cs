﻿using System.ComponentModel.DataAnnotations;
using TaskManagementApplication.API.Enums;

namespace TaskManagementApplication.API.DTOS.TodoTask
{
    public class CreateTodoTaskDto
    {
        [Required(ErrorMessage = "Title is required.")]
        [MaxLength(100, ErrorMessage = "Title cannot exceed 100 characters.")]
        public string Title { get; set; }

        [MaxLength(500, ErrorMessage = "Description cannot exceed 500 characters.")]
        public string Description { get; set; } = string.Empty;
  
        public DateTime? DueDate { get; set; } 

        public Priority Priority { get; set; } 

        public Status Status { get; set; }
    }

}
