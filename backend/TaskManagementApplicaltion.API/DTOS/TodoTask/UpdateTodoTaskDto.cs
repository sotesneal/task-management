﻿using TaskManagementApplication.API.Enums;

namespace TaskManagementApplication.API.DTOS.TodoTask
{
    public class UpdateTodoTaskDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; } = string.Empty;
        public DateTime? DueDate { get; set; } = default(DateTime?);
        public Priority Priority { get; set; } 
        public Status Status { get; set; }
    }
}
