﻿using TaskManagementApplication.API.Enums;

namespace TaskManagementApplication.API.DTOS.TodoTask
{
    public class ReadTodoTaskDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public Priority Priority { get; set; }
        public Status Status { get; set; }
        public string CreatorUserId { get; set; }
        public bool IsArchived { get; set; }
    }
}
