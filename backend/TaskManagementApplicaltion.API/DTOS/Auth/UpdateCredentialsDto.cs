﻿using System.ComponentModel.DataAnnotations;

namespace TaskManagementApplication.API.DTOS.Auth
{
    public class UpdateFirstnameDto
    {
        [Required]
        public string FirstName { get; set; }
    }

    public class UpdateLastnameDto
    {
        [Required]
        public string LastName { get; set; }
    }

    public class UpdateEmailDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

}
