﻿using System.ComponentModel.DataAnnotations;

namespace TaskManagementApplication.API.DTOS.Auth
{
    public class LoginDto
    {
        [Required]
        public string EmailOrUserName { get; set; } = String.Empty;

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
