﻿using System.ComponentModel.DataAnnotations;

namespace TaskManagementApplication.API.DTOS.Auth
{
    public class RegisterDto
    {

        [Required(ErrorMessage = "Username is required.")]
        [MaxLength(20, ErrorMessage = "Username cannot be longer than 20 characters.")]
        [MinLength(4, ErrorMessage = "Username must be at least 4 characters loeng.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [MaxLength(30, ErrorMessage = "First name cannot be longer than 30 characters.")]
        [MinLength(2, ErrorMessage = "First name must be at least 2 characters long.")]
        [RegularExpression(@"^[a-zA-Z\s'-]*$", ErrorMessage = "First name should contain only letters.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [MaxLength(30, ErrorMessage = "Last name cannot be longer than 30 characters.")]
        [MinLength(2, ErrorMessage = "Last name must be at least 2 characters long.")]
        [RegularExpression(@"^[a-zA-Z\s'-]*$", ErrorMessage = "Last name should contain only letters.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid email format.")]
        [MaxLength(254, ErrorMessage = "Email cannot be longer than 254 characters.")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required.")]
        [MinLength(8, ErrorMessage = "Password must be at least 8 characters long.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&+=]).{8,}$",
        ErrorMessage = "Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character.")]
        public string Password { get; set; }

    }
}
