﻿using TaskManagementApplication.API.Enums;
using TaskManagementApplication.API.Models;

namespace TaskManagementApplication.API.DTOS.AuditLog
{
    public class ReadAuditLogDto
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public DateTime Timestamp { get; set; }

        public int TaskId { get; set; } // Foreign key for Task
        public string Username { get; set; } // Foreign key for User
        public string OldValues { get; set; }
        public string NewValues { get; set; }

    }
}
