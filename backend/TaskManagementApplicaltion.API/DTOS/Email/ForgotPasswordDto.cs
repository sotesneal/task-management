﻿namespace TaskManagementApplication.API.DTOS.Email
{
    public class ForgotPasswordDto
    {
        public string Email { get; set; }
    
    }
}
