﻿using TaskManagementApplication.API.DTOS.AuditLog;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories;


namespace TaskManagementApplication.API.Services.Interfaces
{
    public interface IAuditLogService
    {
        Task<IEnumerable<ReadAuditLogDto>> GetAuditLogsAsync( AuditLogFilterOptions filterOptions, string sortBy, bool includeUsername);
    }
}
