﻿using TaskManagementApplication.API.DTOS.TodoTask;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories;

namespace TaskManagementApplication.API.Services.Interfaces
{
    public interface ITodoTaskService
    {
        Task<ReadTodoTaskDto> GetTodoTaskById(int id);
        Task<IEnumerable<ReadTodoTaskDto>> GetAllTodoTask(string userId, TaskFilterOptions filterOptions = null!, string? sortBy = null);
        Task<ReadTodoTaskDto> CreateTodoTask(CreateTodoTaskDto createTodoTaskDto, string username);
        Task<ReadTodoTaskDto> UpdateTodoTask(UpdateTodoTaskDto updateTodoTaskDto);
        Task<bool> DeleteTodoTask(int id);
        Task<TodoTask> ArchiveTaskAsync(int taskId);
        Task<IEnumerable<ReadTodoTaskDto>> GetAllArchivedTasks(string userId);
        Task<IEnumerable<ReadTodoTaskDto>> SearchTasks(string userId, string searchTerm);
        Task<IEnumerable<ReadTodoTaskDto>> SearchArchivedTasks(string userId, string searchTerm);
        Task<bool> UpdateTaskPinnedStatusAsync(int taskId, bool isPinned);
        Task<TodoTask> UnarchiveTaskAsync(int taskId);

    }
}
