﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TaskManagementApplication.API.DTOS.AuditLog;
using TaskManagementApplication.API.Enums;
using TaskManagementApplication.API.Exceptions;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories;
using TaskManagementApplication.API.Repositories.Interfaces;
using TaskManagementApplication.API.Services.Interfaces;

namespace TaskManagementApplication.API.Services
{
    public class AuditLogService : IAuditLogService
    {
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        public AuditLogService(IAuditLogRepository auditLogRepository, IMapper mapper, UserManager<ApplicationUser> userManager)
        {

            _auditLogRepository = auditLogRepository;
            _mapper = mapper;
            _userManager = userManager;

        }
        public async Task<IEnumerable<ReadAuditLogDto>> GetAuditLogsAsync(AuditLogFilterOptions filterOptions = null!, string? sortBy = null, bool includeUsername = false)
        {
            var auditLogs = await _auditLogRepository.GetAuditLogsAsync(filterOptions, sortBy, includeUsername);

            var userIds = auditLogs.Select(log => log.CreatorUserId).Distinct();
            var userIdToUsernameMap = await _userManager.Users.Where(user => userIds.Contains(user.Id)).ToDictionaryAsync(user => user.Id, user => user.UserName);

            var auditLogsDto = auditLogs.Select(auditLog =>
            {
                var username = userIdToUsernameMap.TryGetValue(auditLog.CreatorUserId, out var name) ? name : "Unknown User";

                return new ReadAuditLogDto
                {
                    Id = auditLog.Id,
                    Timestamp = auditLog.Timestamp,
                    TaskId = auditLog.TaskId,
                    Username = username,
                    Action = Enum.GetName(typeof(AuditAction), auditLog.Action)!,
                    OldValues = auditLog.OldValues,
                    NewValues = auditLog.NewValues,

                };

            });

            return auditLogsDto;
        }

    }

}
