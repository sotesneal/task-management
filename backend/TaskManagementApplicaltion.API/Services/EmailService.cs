﻿using SendGrid.Helpers.Mail;
using SendGrid;

namespace TaskManagementApplication.API.Services
{
    public class EmailService
    {
        private readonly IConfiguration _configuration;
        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task SendPasswordResetEmailAsync(string toEmail, string callbackUrl)
        {
            try
            {
                var apiKey = _configuration["SendGridSettings:ApiKey"];
                var senderEmail = _configuration["SendGridSettings:SenderEmail"];

                var client = new SendGridClient(apiKey);
                var msg = new SendGridMessage()
                {
                    From = new EmailAddress(senderEmail, "TaskHub"), // Updated sender name to TaskHub
                    Subject = "Password Reset Request for TaskHub", // Updated email subject
                    PlainTextContent = "Please reset your password by clicking the link below:",
                    HtmlContent = $@"
                        <!DOCTYPE html>
                        <html lang='en'>
                        <head>
                            <meta charset='UTF-8'>
                            <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                            <title>Password Reset Request</title>
                            <style>
                                body {{
                                    font-family: Arial, sans-serif;
                                    background-color: #f4f4f4;
                                    margin: 0;
                                    padding: 0;
                                }}
                                .container {{
                                    max-width: 600px;
                                    margin: 0 auto;
                                    padding: 20px;
                                    background-color: #ffffff;
                                    border-radius: 5px;
                                    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                                }}
                                h1 {{
                                    color: #333;
                                }}
                                p {{
                                    color: #555;
                                }}
                                a.button {{
                                    display: inline-block;
                                    padding: 10px 20px;
                                    margin-top: 20px;
                                    background-color: #007BFF;
                                    color: #fff;
                                    border: none;
                                    border-radius: 5px;
                                    text-decoration: none;
                                    text-align: center;
                                    cursor: pointer;
                                }}
                                a.button:hover {{
                                    background-color: #0056b3; /* Change color on hover */
                                }}
                            </style>
                        </head>
                        <body>
                            <div class='container'>
                                <h1>Password Reset Request</h1>
                                <p>
                                    You have requested to reset your password for TaskHub. Please click the button below to reset your password.
                                </p>
                                <a href='{callbackUrl}' target='_blank' class='button'>Reset Password</a>
                                <p>If you did not request this password reset, please ignore this email. Your account's security is important to us.</p>
                            </div>
                        </body>
                        </html>"

                };

                msg.AddTo(new EmailAddress(toEmail));

                await client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task SendContactUsEmailAsync(string toEmail, string subject, string message)
        {
            try
            {
                var apiKey = _configuration["SendGridSettings:ApiKey"];
                var senderEmail = _configuration["SendGridSettings:SenderEmail"];

                var client = new SendGridClient(apiKey);
                var msg = new SendGridMessage()
                {
                    From = new EmailAddress(senderEmail, "TaskHub"),
                    Subject = subject,
                    HtmlContent = $@"
                <!DOCTYPE html>
                <html lang='en'>
                <head>
                    <meta charset='UTF-8'>
                    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                    <title>{subject}</title>
                    <style>
                        body {{
                            font-family: Arial, sans-serif;
                            background-color: #f4f4f4;
                            margin: 0;
                            padding: 0;
                        }}
                        .container {{
                            max-width: 600px;
                            margin: 0 auto;
                            padding: 20px;
                            background-color: #ffffff;
                            border-radius: 5px;
                            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                        }}
                        h1 {{
                            color: #333;
                        }}
                        p {{
                            color: #555;
                        }}
                        a.button {{
                            display: inline-block;
                            padding: 10px 20px;
                            margin-top: 20px;
                            background-color: #007BFF;
                            color: #fff;
                            border: none;
                            border-radius: 5px;
                            text-decoration: none;
                            text-align: center;
                            cursor: pointer;
                        }}
                        a.button:hover {{
                            background-color: #0056b3;
                        }}
                        /* Custom styles */
                        .custom-message {{
                            font-size: 16px;
                            line-height: 1.5;
                            margin-top: 20px;
                            color: #333;
                        }}
                    </style>
                </head>
                <body>
                    <div class='container'>
                        <h1>{subject}</h1>
                        <p class='custom-message'>{message}</p>
                        <a href='#' class='button'>Contact Us</a>
                    </div>
                </body>
                </html>"
                };

                msg.AddTo(new EmailAddress(toEmail));

                await client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
