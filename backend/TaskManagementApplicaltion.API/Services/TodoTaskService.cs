﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManagementApplication.API.DTOS.TodoTask;
using TaskManagementApplication.API.Models;
using TaskManagementApplication.API.Repositories;
using TaskManagementApplication.API.Repositories.Interfaces;
using TaskManagementApplication.API.Services.Exceptions;
using TaskManagementApplication.API.Services.Interfaces;

namespace TaskManagementApplication.API.Services
{
    public class TodoTaskService : ITodoTaskService
    {
        private readonly ITodoTaskRepository _todoTaskRepository;
        private readonly IMapper _mapper;

        public TodoTaskService(ITodoTaskRepository todoTaskRepository, IMapper mapper)
        {
            _todoTaskRepository = todoTaskRepository;
            _mapper = mapper;
        }

        public async Task<TodoTask> ArchiveTaskAsync(int taskId)
        {
            var task = await _todoTaskRepository.GetTaskByIdAsync(taskId);
            if (task == null)
            {
                throw new NotFoundException($"Task with ID {taskId} not found.");
            }
            if (task.IsArchived)
            {
                throw new InvalidOperationException($"Task with ID {taskId} is already archived.");
            }
            await _todoTaskRepository.ArchiveAsync(task);
            return task;
        }

        public async Task<ReadTodoTaskDto> CreateTodoTask(CreateTodoTaskDto createTodoTaskDto, string userId)
        {
            if (createTodoTaskDto == null)
            {
                throw new ArgumentNullException(nameof(createTodoTaskDto));
            }


            var todoTask = _mapper.Map<TodoTask>(createTodoTaskDto);
            todoTask.CreatorUserId = userId;
            var createdTodoTask = await _todoTaskRepository.CreateTaskAsync(todoTask);

            return _mapper.Map<ReadTodoTaskDto>(createdTodoTask);
        }

        public async Task<bool> DeleteTodoTask(int id)
        {
            return await _todoTaskRepository.DeleteTaskAsync(id);
        }

        public async Task<IEnumerable<ReadTodoTaskDto>> GetAllArchivedTasks(string userId)
        {
            var archivedTasks = await _todoTaskRepository.GetAllArchivedTasksAsync(userId);
            return _mapper.Map<IEnumerable<ReadTodoTaskDto>>(archivedTasks);
        }

        public async Task<IEnumerable<ReadTodoTaskDto>> GetAllTodoTask(string userId,TaskFilterOptions filterOptions = null!, string? sortBy = null)
        {
            var todoTasks = await _todoTaskRepository.GetAllTasksAsync(userId, filterOptions, sortBy);
            return _mapper.Map<IEnumerable<ReadTodoTaskDto>>(todoTasks);
        }

       

        public async Task<ReadTodoTaskDto> GetTodoTaskById(int id)
        {
            var todoTask = await _todoTaskRepository.GetTaskByIdAsync(id);

            return todoTask == null ? throw new NotFoundException($"TodoTask with ID {id} not found") : _mapper.Map<ReadTodoTaskDto>(todoTask);
        }

        public async Task<ReadTodoTaskDto> UpdateTodoTask(UpdateTodoTaskDto updateTodoTaskDto)
        {
            if (updateTodoTaskDto == null)
            {
                throw new ArgumentNullException(nameof(updateTodoTaskDto));
            }

            var existingTodoTask = await _todoTaskRepository.GetTaskByIdAsync(updateTodoTaskDto.Id) ?? throw new NotFoundException($"TodoTask with ID {updateTodoTaskDto.Id} not found");
            _mapper.Map(updateTodoTaskDto, existingTodoTask);
            var updatedTodoTask = await _todoTaskRepository.UpdateTaskAsync(existingTodoTask);

            return _mapper.Map<ReadTodoTaskDto>(updatedTodoTask);
        }
        public async Task<IEnumerable<ReadTodoTaskDto>> SearchTasks(string userId, string searchTerm)
        {
            // Perform the task search based on the searchTerm
            var searchResults = await _todoTaskRepository.SearchTasksAsync(userId, searchTerm);

            // Map the search results to DTOs as needed
            var searchResultDtos = _mapper.Map<IEnumerable<ReadTodoTaskDto>>(searchResults);

            return searchResultDtos;
        }

        public async Task<IEnumerable<ReadTodoTaskDto>> SearchArchivedTasks(string userId, string searchTerm)
        {
            // Perform the search for archived tasks based on the searchTerm
            var searchResults = await _todoTaskRepository.SearchArchivedTasksAsync(userId, searchTerm);

            // Map the search results to DTOs as needed
            var searchResultDtos = _mapper.Map<IEnumerable<ReadTodoTaskDto>>(searchResults);

            return searchResultDtos;
        }


        public async Task<bool> UpdateTaskPinnedStatusAsync(int taskId, bool isPinned)
        {
            return await _todoTaskRepository.UpdateTaskPinnedStatusAsync(taskId, isPinned);
        }

        public async Task<TodoTask> UnarchiveTaskAsync(int taskId)
        {
            var existingTask = await _todoTaskRepository.GetTaskByIdAsync(taskId);

            if (existingTask == null)
            {
                throw new NotFoundException("Task not found");
            }

            // Call the repository method with the unarchive parameter set to true
            return await _todoTaskRepository.UpdateTaskAsync(existingTask, unarchive: true);
        }
    }
}
