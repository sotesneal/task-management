import axios from 'axios';

const BASE_URL = 'http://localhost:5228/api/account'; // Replace with your API base URL

export const AuthService = {
  register: async (formData) => {
    try {
      await axios.post(`${BASE_URL}/register`, formData);
      return {}; // Return an empty object for successful registration
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data;
      } else {
        throw new Error('Oops! Something went wrong. Please check your internet connection and try again later.');
      }
    }
  },
  login: async (formData) => {
    try {
      const response = await axios.post(`${BASE_URL}/login`, formData);
      return response.data.token;
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data; 
      } else {
        throw new Error('Oops! Something went wrong. Please check your internet connection and try again later.');
      }
    
    }
  },
  getUserInfo: async (token) => {
    try {
      const response = await axios.get(`${BASE_URL}/userinfo`, {
        headers: { 
          'Authorization': `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      });
  
      return response.data;
    } catch (error) {
      if (error.response && error.response.data) {
        throw new Error(error.response.data); 
      } else {
        throw new Error('Failed to fetch user info.');
      }
    }
  },
  forgotPassword: async (formData) => {
    try {
      const response = await axios.post(`${BASE_URL}/forgot-password`, formData);
      return response.data; // Return a success message or response data
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data; // Return error details from the backend
      } else {
        throw new Error('Oops! Something went wrong. Please check your internet connection and try again later.');
      }
    }
  },
  resetPassword: async (formData) => {
    try {
      const response = await axios.post(`${BASE_URL}/reset-password`, formData);
      return response.data; // Return a success message or response data
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data; // Return error details from the backend
      } else {
        throw new Error('Oops! Something went wrong. Please check your internet connection and try again later.');
      }
    }
  },
  logout: async () => {
    try {
      await axios.post(`${BASE_URL}/logout`);
    } catch (error) {
      // Handle any logout-related errors (optional)
      console.error('Logout failed:', error);
    }
  },
  uploadProfilePicture: async (token, file) => {
    try {
      // Create a FormData object to send the file
      const formData = new FormData();
      formData.append('file', file);

      // Make a POST request to the upload-profile-picture endpoint
      const response = await axios.post(`${BASE_URL}/upload-profile-picture`, formData, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'multipart/form-data', // Important for sending files
        },
      });

      return response.data.ProfilePictureUrl;
    } catch (error) {
      if (error.response && error.response.data) {
        throw new Error(error.response.data); // Return error details from the backend
      } else {
        throw new Error('Oops! Something went wrong. Please check your internet connection and try again later.');
      }
    }
  },
  changePassword: async (token, formData) => {
    try {
      const response = await axios.post(
        `${BASE_URL}/change-password`,
        formData,
        {
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        }
      );

      return response.data; // Return a success message or response data
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data; // Return error details from the backend
      } else {
        throw new Error('Oops! Something went wrong. Please check your internet connection and try again later.');
      }
    }
  },
  updateFirstname: async (token, newFirstname) => {
    try {
      const response = await axios.put(
        `${BASE_URL}/update-firstname`,
        { FirstName: newFirstname },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        }
      );
      return response.data; // Return a success message or response data
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data; // Return error details from the backend
      } else {
        throw new Error(
          'Oops! Something went wrong. Please check your internet connection and try again later.'
        );
      }
    }
  },
  
  updateLastname: async (token, newLastname) => {
    try {
      const response = await axios.put(
        `${BASE_URL}/update-lastname`,
        { LastName: newLastname },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        }
      );
      return response.data; // Return a success message or response data
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data; // Return error details from the backend
      } else {
        throw new Error(
          'Oops! Something went wrong. Please check your internet connection and try again later.'
        );
      }
    }
  },
  updateEmail: async (token, newEmail) => {
    try {
      const response = await axios.put(
        `${BASE_URL}/update-email`,
        { Email: newEmail },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
        }
      );
      return response.data; // Return a success message or response data
    } catch (error) {
      if (error.response && error.response.data) {
        return error.response.data; // Return error details from the backend
      } else {
        throw new Error(
          'Oops! Something went wrong. Please check your internet connection and try again later.'
        );
      }
    }
  },
  
  
};
