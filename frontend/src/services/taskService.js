import axios from "axios";

const API_BASE_URL = "http://localhost:5228/api"; // Replace with your actual API URL

const taskService = {
  getAllTasks: async (token) => {
    try {
      const response = await axios.get(`${API_BASE_URL}/todotasks`, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      });
      return response.data;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  addTask: async (token, newTask) => {
    try {
      const response = await axios.post(`${API_BASE_URL}/todotasks`, newTask, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      });
      return response;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  removeTask: async (token, taskId) => {
    try {
      const response = await axios.delete(
        `${API_BASE_URL}/todotasks/${taskId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      );
      return response;
    } catch (error) {
      throw error;
    }
  },
  updateTask: async (token, taskId, updatedTask) => {
    try {
      const response = await axios.put(
        `${API_BASE_URL}/todotasks/${taskId}`,
        updatedTask,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      );
      return response.data;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  archiveTask: async (token, taskId) => {
    try {
      const response = await axios.put(
        `${API_BASE_URL}/todotasks/archive/${taskId}`,
        null,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      );
      return response;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  searchTasks: async (token, searchQuery) => {
    try {
      const response = await axios.get(`${API_BASE_URL}/todotasks/search`, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        params: {
          searchTerm: searchQuery, // Pass the search query as a parameter
        },
      });
      return response.data;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  searchArchivedTasks: async (token, searchQuery) => {
    try {
      const response = await axios.get(`${API_BASE_URL}/TodoTasks/archive/search`, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        params: {
          searchTerm: searchQuery, // Pass the search query as a parameter
        },
      });
      return response.data;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  updateTaskPinnedStatus: async (token, taskId, isPinned) => {
    try {
      const response = await axios.put(
        `${API_BASE_URL}/todotasks/${taskId}/pin`,
        isPinned, // Send the isPinned value in the request body
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      );
      return response.data;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  getArchivedTasks: async (token) => {
    try {
      const response = await axios.get(`${API_BASE_URL}/todotasks/archive`, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      });
      return response.data;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
  unarchiveTask: async (token, taskId) => {
    try {
      const response = await axios.post(
        `${API_BASE_URL}/todotasks/unarchive/${taskId}`,
        null,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      );
      return response.data;
    } catch (error) {
      // Handle errors here (e.g., network error, server error)
      throw error;
    }
  },
};

export default taskService;
