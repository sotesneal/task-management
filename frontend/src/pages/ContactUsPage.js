import React from "react";
import Header from "../../src/components/Header";
import ContactUs from "../components/ContactUs";

const ContactUsPage = () => {
  return (
    <div>
      <Header />
      <div className="w-full max-w-md mx-auto">
        <ContactUs />
      </div>
    </div>
  );
};

export default ContactUsPage;
