import React from 'react';
import SyncLoader from 'react-spinners/SyncLoader';

const LoadingSpinner = () => {
    console.log('Loading Spinner Rendered');
    return (
      <SyncLoader
       
        size={8}
        color={'#0079bf'}
      />
    );
  };
  
export default LoadingSpinner;
