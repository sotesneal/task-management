import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import taskService from "../services/taskService";
import TaskCard from "../components/TaskCard";
import { toast } from "react-toastify";
import UserDashboardHeader from "../components/UserDashboardHeader";
import { PencilAltIcon } from "@heroicons/react/solid";

const Container = styled.div`
  display: flex;
  justify-content: space-evenly;
  overflow-x: hidden;
  width: 100%;
`;

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: 14px;
  width: 100%;
  /* Hover effect */
  &:hover {
    background-color: #f7f8f9;
  }
`;

const TaskColumnStyles = styled.div`
  min-width: 341px;
  display: flex;
  flex-direction: column;
  background: #fafbfc;
`;

const AddCardButton = styled.button`
  border: none;
  text-align: left;
  padding: 10px 10px;
  margin: 5px;
  width: 93%;
  heigth: auto;
  color: #333;
  cursor: pointer;
  border-radius: 10px;
  font-size: 14px;
  transition: background 0.2s ease-in-out;
  &:hover {
    background: #fff;
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
    border-radius: 10px;
  }
  bg-blue-200 text-blue-600 hover:bg-blue-300 hover:text-blue-700
`;

const InputField = styled.div`
  display: ${(props) => (props.showInput ? "block" : "none")};
  background: #ffffff;
  border: 1px solid #ccc; 
  width: 90%;
  border-radius: 10px;
  padding: 10px;
  margin: 10px 0;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  
  /* Trello-like styling */
  position: relative;
  transition: background 0.2s ease-in-out;
  
  &:hover {
    background: #F7F8F9
  }

  /* Style the input field inside the div */
  input {
    border: 0.2px solid;
    border-color: #D3D3D3;
    width: 100%;
    padding: 15px 10px;
    border-radius: 10px;
    font-size: 14px;
    outline: none;
    
  }

  /* Style the buttons */
  button {
    background: none;
    border: none;
    color: #0079bf; /* Trello blue color */
    font-weight: bold;
    cursor: pointer;
    margin-top: 10px;
    margin-right: 5px;
    padding: 5px 10px;
    border-radius: 3px;
    transition: background 0.2s ease-in-out, color 0.2s ease-in-out;

    &:hover {
      background: rgba(0, 121, 191, 0.1); 
      border-radius: 10px
    }
  }
  bg-white border border-gray-300 rounded-md p-2 mt-2 shadow-md transition duration-300 hover:bg-gray-100

`;

const ScrollableContent = styled.div`
  overflow-y: auto;
  max-height: calc(74vh - 105px);
  display: flex;
  flex-direction: column;
  min-width: 341px;
  border-radius: 5px;
  padding: 10px 10px;
`;

const Title = styled.span`
  color: #fff;
  font-weight: bold;
  background: #5850ec;
  padding: 5px 10px;
  border-radius: 5px;
  align-self: flex-start;
  position: sticky;
  width: 100%;
  top: 0;
  z-index: 1;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
`;

const TaskPage = () => {
  const [columns, setColumns] = useState({
    todo: { title: "Todo", items: [] },
    inProgress: { title: "In Progress", items: [] },
    done: { title: "Done", items: [] },
  });
  const [tasks, setTasks] = useState([]);
  const [newCardTitles, setNewCardTitles] = useState({
    todo: "",
    inProgress: "",
    done: "",
  });

  const [, setAddingCardToColumn] = useState(null);
  const [editingTask, setEditingTask] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [currentColumn, setCurrentColumn] = useState();
  const [editable, setEditable] = useState(false);
  const [taskUnarchived, setTaskUnarchived] = useState();
  const [taskUnarchivedId, setTaskUnarchivedId] = useState();
  const [, setSelectedTask] = useState(null);
  const [validationErrors, setValidationErrors] = useState({
    title: "",
    description: "",
  });

  useEffect(() => {
    if (showModal) {
      // Reset the editable state when the modal is opened
      setEditable(false);
    }
  }, [showModal]);
  const toggleModal = (task, columnId) => {
    setValidationErrors({
      title: "",
      description: "",
    });
    setEditingTask(
      task
        ? {
            ...editingTask,
            title: task.title,
            status: task.status,
            description: task.description,
            dueDate: task.dueDate,
            priority: task.priority,
            id: task.id,
          }
        : null
    );
    setShowModal(!showModal);
    setCurrentColumn(columnId);
  };

  const handleUpdateTask = async () => {
    try {
      const MAX_TITLE_LENGTH = 50;
      const MAX_DESCRIPTION_LENGTH = 200;
      const errors = {};

      // Check if the title is empty
      if (!editingTask.title.trim()) {
        errors.title = "Title cannot be empty";
      } else if (editingTask.title.length > MAX_TITLE_LENGTH) {
        // Check if the title exceeds the maximum length
        errors.title = `Title cannot exceed ${MAX_TITLE_LENGTH} characters`;
      }

      // Check if the description exceeds the maximum length
      if (
        editingTask.description &&
        editingTask.description.length > MAX_DESCRIPTION_LENGTH
      ) {
        errors.description = `Description cannot exceed ${MAX_DESCRIPTION_LENGTH} characters`;
      }

      // If there are errors, set them in the state and prevent saving
      if (Object.keys(errors).length > 0) {
        setValidationErrors(errors);
        return;
      }

      const tokenString = localStorage.getItem("token");
      const authToken = JSON.parse(tokenString);

      // Assuming currentColumn is correctly defined and has a valid value
      const currentStatus = getStatusFromColumnId(currentColumn);

      // Check if editingTask.dueDate is falsy, and set the default value if true
      const formattedDueDate = editingTask.dueDate || null; // Default due date value

      console.log("current status " + currentColumn);
      console.log("current status " + currentStatus);
      const updatedTask = {
        id: editingTask.id,
        title: editingTask.title,
        status: currentStatus,
        description: editingTask.description,
        dueDate: formattedDueDate,
        priority: parseInt(editingTask.priority),
      };

      // Update the task on the server
      await taskService.updateTask(authToken, editingTask.id, updatedTask);

      // Assuming columns is an object with keys representing columns
      setColumns((prevColumns) => {
        const updatedColumns = { ...prevColumns };
        const column = updatedColumns[currentColumn];

        // Check if column and column.items are defined
        if (column && column.items) {
          const updatedItems = column.items.map((item) => {
            if (item.id === editingTask.id) {
              return { ...item, ...updatedTask };
            }
            return item;
          });
          column.items = updatedItems;
        }

        return updatedColumns;
      });

      // Update the task in the state with the updated data
      const updatedTasks = tasks.map((task) => {
        if (task.id === editingTask.id) {
          return { ...task, ...updatedTask };
        }
        return task;
      });

      setTasks(updatedTasks);

      toast.success("Task updated!", { autoClose: 4000 });
      // Close the modal
      toggleModal(updatedTask);
    } catch (error) {
      console.error("Task update failed:", error);
      toast.error("Error updating the task.", { autoClose: 2000 });
    }
  };

  const handleRemoveTask = async (taskId) => {
    const updatedTasks = tasks.filter((task) => task.id !== taskId);
    setTasks(updatedTasks);
  };

  const [inputVisibleMap, setInputVisibleMap] = useState({
    todo: false,
    inProgress: false,
    done: false,
  });
  useEffect(() => {
    async function fetchAndOrganizeTasks() {
      try {
        const tokenString = localStorage.getItem("token");
        const authToken = JSON.parse(tokenString);
        const tasksData = await taskService.getAllTasks(authToken);

        // Map the tasks data and include an 'isArchived' property
        const organizedTasks = tasksData.map((task) => ({
          ...task,
          isArchived: false, // Assuming none of the fetched tasks are initially archived
        }));
        const organizedColumns = {
          todo: { title: "Todo", items: [] },
          inProgress: { title: "In Progress", items: [] },
          done: { title: "Done", items: [] },
        };

        tasksData.forEach((task) => {
          switch (task.status) {
            case 0:
              organizedColumns.todo.items.push(task);
              break;
            case 1:
              organizedColumns.inProgress.items.push(task);
              break;
            case 2:
              organizedColumns.done.items.push(task);
              break;
            default:
              break;
          }
        });

        setColumns(organizedColumns);
        setTasks(organizedTasks);
      } catch (error) {
        console.error("Error fetching tasks:", error);
      }
    }

    fetchAndOrganizeTasks();
  }, []);
  const getStatusLabel = (status) => {
    switch (status) {
      case "todo":
        return "Todo";
      case "inProgress":
        return "In Progress";
      case "done":
        return "Done";
      default:
        return "";
    }
  };
  const getStatusCode = (status) => {
    switch (status) {
      case 0:
        return "Todo";
      case 1:
        return "In Progress";
      case 2:
        return "Done";
      default:
        return "";
    }
  };

  const onDragEnd = async (result) => {
    if (!result.destination) return;

    const { source, destination, draggableId } = result;
    const sourceColumnId = source.droppableId;
    const destinationColumnId = destination.droppableId;

    // Get the task that was dragged
    const draggedTask = columns[sourceColumnId].items[source.index];

    // Check if the task was dropped in a different column
    if (sourceColumnId !== destinationColumnId) {
      // Calculate the new status based on the destination column
      const newStatus = getStatusFromColumnId(destinationColumnId);

      // Update the task's status locally
      const newColumns = { ...columns };
      newColumns[sourceColumnId].items.splice(source.index, 1);
      newColumns[destinationColumnId].items.splice(
        destination.index,
        0,
        draggedTask
      );
      setColumns(newColumns);

      // Make an API request to update the task's status
      try {
        const tokenString = localStorage.getItem("token");
        const authToken = JSON.parse(tokenString);

        // Ensure that the status is correctly updated in the task object
        const updatedTask = { ...draggedTask, status: newStatus };

        await taskService.updateTask(authToken, draggableId, updatedTask);

        toast.success("Task status updated!", {
          autoClose: 2000,
        });
      } catch (error) {
        console.error("Error updating task status:", error);
        // Handle errors (e.g., network error, server error) here
      }
    } else {
      // Reorder the cards within the same column
      const newColumns = { ...columns };
      const sourceItems = newColumns[sourceColumnId].items;
      sourceItems.splice(source.index, 1); // Remove the dragged task from its original position
      sourceItems.splice(destination.index, 0, draggedTask); // Insert the task at the new position
      setColumns(newColumns);
    }
  };
  const openModalWithTask = (task, columnId) => {
    toggleModal(task, columnId);
    setSelectedTask(task);
    setShowModal(!showModal);
  };

  const handleAddCard = (columnId) => {
    setInputVisibleMap((prevInputVisibleMap) => ({
      ...prevInputVisibleMap,
      [columnId]: true,
    }));
    setNewCardTitles((prevNewCardTitles) => ({
      ...prevNewCardTitles,
      [columnId]: "",
    }));
    setAddingCardToColumn(columnId);
  };

  const handleCancelAddCard = (columnId) => {
    setInputVisibleMap((prevInputVisibleMap) => ({
      ...prevInputVisibleMap,
      [columnId]: false,
    }));
    setNewCardTitles((prevNewCardTitles) => ({
      ...prevNewCardTitles,
      [columnId]: "",
    }));
  };
  // Function to format the date as "YYYY-MM-DD"
  const formatDate = (dateString) => {
    if (!dateString) return ""; // Handle cases where the date is falsy
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0"); // Month is zero-based
    const day = String(date.getDate()).padStart(2, "0");
    return `${year}-${month}-${day}`;
  };

  const getStatusFromColumnId = (columnId) => {
    switch (columnId) {
      case "todo":
        return 0; // Map to the status value for Todo
      case "inProgress":
        return 1; // Map to the status value for In Progress
      case "done":
        return 2; // Map to the status value for Done
      default:
        return -1; // Handle any other cases or invalid column IDs
    }
  };

  const getPriorityLabel = (priority) => {
    switch (priority) {
      case "Low":
        return 1;
      case "Medium":
        return 2;
      case "High":
        return 3;
      default:
        return 0;
    }
  };

  const handleAddCardSubmit = async (columnId) => {
    try {
      const newTask = {
        title: newCardTitles[columnId],
        status: getStatusFromColumnId(columnId),
      };

      // Check if a priority has been selected, and if so, add it to the newTask object
      if (newCardTitles.priority) {
        newTask.priority = getPriorityLabel(newCardTitles.priority);
      }

      const tokenString = localStorage.getItem("token");
      const authToken = JSON.parse(tokenString);

      const response = await taskService.addTask(authToken, newTask);

      if (response.status === 201) {
        const newTaskData = response.data;

        const updatedColumns = { ...columns };
        updatedColumns[columnId].items.push(newTaskData);
        setColumns(updatedColumns);

        setInputVisibleMap((prevInputVisibleMap) => ({
          ...prevInputVisibleMap,
          [columnId]: false,
        }));
        setNewCardTitles((prevNewCardTitles) => ({
          ...prevNewCardTitles,
          [columnId]: "",
          priority: "", // Reset priority to empty string after adding the task
        }));
        toast.success("Task added!", { autoClose: 2000 });
      } else {
        // Handle error responses if needed
      }
    } catch (error) {
      console.error("Error adding task:", error);
      // Handle error responses if needed
    }
  };
  const handlePinToggle = (taskId) => {
    // Toggle the pinned status in local storage
    const isPinned = localStorage.getItem(`pinned_${taskId}`) === "true";
    localStorage.setItem(`pinned_${taskId}`, (!isPinned).toString());

    // Update the tasks and columns with the new pinned status
    setTasks((prevTasks) => {
      return prevTasks.map((task) => {
        if (task.id === taskId) {
          return { ...task, isPinned: !isPinned };
        }
        return task;
      });
    });

    setColumns((prevColumns) => {
      // Clone the columns object to avoid mutating state directly
      const updatedColumns = { ...prevColumns };
      // Update each column's items array to reflect the new order
      for (const columnId in updatedColumns) {
        const column = updatedColumns[columnId];
        const updatedItems = [...column.items];
        updatedItems.sort((a, b) => {
          if (a.isPinned && !b.isPinned) {
            return -1;
          } else if (!a.isPinned && b.isPinned) {
            return 1;
          } else {
            // Keep the original order for tasks with the same pinned status
            return a.originalIndex - b.originalIndex;
          }
        });
        column.items = updatedItems;
      }
      return updatedColumns;
    });
  };
  const sortTasks = (tasks) => {
    // Separate tasks into pinned and unpinned arrays
    const pinnedTasks = tasks.filter((task) => localStorage.getItem(`pinned_${task.id}`) === "true");
    const unpinnedTasks = tasks.filter((task) => localStorage.getItem(`pinned_${task.id}`) !== "true");
  
    // Sort tasks within each group
    const compareTasks = (a, b) => {
      const aPinned = localStorage.getItem(`pinned_${a.id}`) === "true";
      const bPinned = localStorage.getItem(`pinned_${b.id}`) === "true";
  
      if (aPinned && !bPinned) {
        return -1;
      } else if (!aPinned && bPinned) {
        return 1;
      } else {
        return a.originalIndex - b.originalIndex;
      }
    };
  
    pinnedTasks.sort(compareTasks);
    unpinnedTasks.sort(compareTasks);
  
    // Combine the sorted arrays
    return [...pinnedTasks, ...unpinnedTasks];
  };
  

  const updateTaskOnUnarchive = (taskId) => {
    // Create a copy of the tasks array with the updated task
    const updatedTasks = tasks.map((task) => {
      if (task.id === taskId) {
        task.isArchived = false;
        localStorage.setItem(`archived_${taskId}`, "false");
        setTaskUnarchived(false);
        console.log("Unarchived " + taskUnarchived);
        setTaskUnarchivedId(taskId);
      }
      return task;
    });

    // Set the updated tasks state to trigger a re-render
    setTasks(updatedTasks);
  };

  useEffect(() => {
    if (showModal) {
      // Reset the editable state when the modal is opened
      setEditable(false);
    }
  }, [showModal]);

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <UserDashboardHeader
        currentColumn={currentColumn}
        openModalWithTask={openModalWithTask}
        updateTaskOnUnarchive={updateTaskOnUnarchive}
        onDeleteTask={handleRemoveTask}
      />
      <Container>
        {Object.entries(columns).map(([columnId, column]) => (
          <TaskColumnStyles key={columnId}>
            <Title>{column.title}</Title>
            <Droppable droppableId={columnId}>
              {(provided) => (
                <ColumnContainer>
                  <ScrollableContent
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                  >
                    {sortTasks(column.items).map((task, taskIndex) => (
                      <TaskCard
                        key={task.id}
                        item={task}
                        index={taskIndex}
                        onRemove={handleRemoveTask}
                        columns={columns}
                        columnId={columnId}
                        updateColumns={setColumns}
                        toggleModal={toggleModal}
                        editingTaskId={editingTask?.id}
                        editingTaskTitle={editingTask?.title}
                        editingTaskDesc={editingTask?.description}
                        onPinToggle={handlePinToggle}
                        taskUnarchived={taskUnarchived}
                        taskUnarchivedId={taskUnarchivedId}
                      />
                    ))}
                    {provided.placeholder}
                  </ScrollableContent>
                  {inputVisibleMap[columnId] ? (
                    <InputField showInput={inputVisibleMap[columnId]}>
                      <input
                        type="text"
                        value={newCardTitles[columnId]}
                        onChange={(e) =>
                          setNewCardTitles((prevNewCardTitles) => ({
                            ...prevNewCardTitles,
                            [columnId]: e.target.value,
                          }))
                        }
                        maxLength={50}
                      />
                      <button onClick={() => handleAddCardSubmit(columnId)}>
                        Add
                      </button>
                      <button onClick={() => handleCancelAddCard(columnId)}>
                        Cancel
                      </button>
                    </InputField>
                  ) : (
                    <AddCardButton onClick={() => handleAddCard(columnId)}>
                      + Add Task
                    </AddCardButton>
                  )}
                </ColumnContainer>
              )}
            </Droppable>
          </TaskColumnStyles>
        ))}
      </Container>
      {showModal && (
        <div className="fixed inset-0 flex items-center justify-center z-50 backdrop-blur-sm backdrop-filter">
          <div className="modal-container bg-white w-96 rounded-lg shadow-md p-6">
            <h2 className="text-lg font-semibold mb-4">Details</h2>
            {editingTask && (
              <p className="text-sm text-gray-600 mb-2">
                In list:{" "}
                {getStatusLabel(currentColumn) ||
                  getStatusCode(editingTask.status)}
              </p>
            )}
            <div className="mb-4">
              <label className="text-sm text-gray-600">Title:</label>
              <input
                type="text"
                value={editingTask ? editingTask.title : ""}
                onChange={(e) =>
                  setEditingTask({ ...editingTask, title: e.target.value })
                }
                className={`w-full border border-gray-300 rounded-lg p-2 text-sm ${
                  !editable ? "bg-gray-100" : ""
                }`}
                disabled={!editable}
              />
              {validationErrors.title && (
                <p className="text-red-500 text-xs">{validationErrors.title}</p>
              )}
            </div>
            <div className="mb-4">
              <label className="text-sm text-gray-600">Description:</label>
              <textarea
                value={editingTask?.description || ""}
                onChange={(e) =>
                  setEditingTask({
                    ...editingTask,
                    description: e.target.value,
                  })
                }
                className={`w-full border border-gray-300 rounded-lg p-2 text-sm ${
                  !editable ? "bg-gray-100" : ""
                }`}
                rows="4"
                disabled={!editable}
              ></textarea>
              {validationErrors.description && (
                <p className="text-red-500 text-xs">
                  {validationErrors.description}
                </p>
              )}
            </div>
            <div className="mb-4">
              <label className="text-sm text-gray-600">Due Date:</label>
              <input
                type="date"
                value={formatDate(editingTask?.dueDate)}
                onChange={(e) =>
                  setEditingTask({ ...editingTask, dueDate: e.target.value })
                }
                className={`w-full border border-gray-300 rounded-lg p-2 text-sm ${
                  !editable ? "bg-gray-100" : ""
                }`}
                disabled={!editable}
              />
            </div>
            <div className="mb-4">
              <label className="text-sm text-gray-600">Priority:</label>
              <select
                value={editingTask?.priority}
                onChange={(e) =>
                  setEditingTask({ ...editingTask, priority: e.target.value })
                }
                className={`w-full border border-gray-300 rounded-lg p-2 text-sm ${
                  !editable ? "bg-gray-100" : ""
                }`}
                disabled={!editable}
              >
                <option value="">---Select Priority---</option>
                <option value="0">None</option>
                <option value="1">Low</option>
                <option value="2">Medium</option>
                <option value="3">High</option>
              </select>
            </div>
            <div className="flex justify-end">
              {editable ? (
                <button
                  onClick={handleUpdateTask}
                  className={`bg-blue-500 text-white font-semibold px-4 py-2 rounded-lg mr-2 hover:bg-blue-600 ${
                    !editable ? "hidden" : ""
                  }`}
                >
                  Save
                </button>
              ) : (
                <button
                  onClick={() => setEditable(true)}
                  className="bg-green-500 text-white font-semibold px-4 py-2 rounded-lg mr-2 hover:bg-green-600"
                >
                  <PencilAltIcon className="w-5 h-5 inline-block -ml-1" /> Edit
                </button>
              )}
              <button
                onClick={toggleModal}
                className="bg-gray-300 text-gray-700 font-semibold px-4 py-2 rounded-lg hover:bg-gray-400"
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      )}
    </DragDropContext>
  );
};

export default TaskPage;
