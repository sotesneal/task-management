// TaskModal.js
import React from 'react';

const TaskModal = ({ isOpen, onClose, task, onUpdate }) => {
  if (!isOpen || !task) {
    return null;
  }

  const handleTitleChange = (e) => {
    onUpdate({ ...task, title: e.target.value });
  };

  const handleDescriptionChange = (e) => {
    onUpdate({ ...task, description: e.target.value });
  };

  const handleDueDateChange = (e) => {
    onUpdate({ ...task, dueDate: e.target.value });
  };

  return (
    <div className="modal">
      <div className="modal-content">
        <h2>Edit Task</h2>
        <div className="mb-4">
          <label className="text-sm text-gray-600">Title:</label>
          <input
            type="text"
            value={task.title}
            onChange={handleTitleChange}
            className="w-full border border-gray-300 rounded-lg p-2 text-sm focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="mb-4">
          <label className="text-sm text-gray-600">Description:</label>
          <textarea
            value={task.description}
            onChange={handleDescriptionChange}
            className="w-full border border-gray-300 rounded-lg p-2 text-sm focus:outline-none focus:border-blue-500"
            rows="4"
          ></textarea>
        </div>
        <div className="mb-4">
          <label className="text-sm text-gray-600">Due Date:</label>
          <input
            type="date"
            value={task.dueDate}
            onChange={handleDueDateChange}
            className="w-full border border-gray-300 rounded-lg p-2 text-sm focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="flex justify-end">
          <button
            onClick={() => {
              onUpdate(task);
              onClose();
            }}
            className="bg-blue-500 text-white font-semibold px-4 py-2 rounded-lg mr-2 hover:bg-blue-600"
          >
            Save
          </button>
          <button
            onClick={onClose}
            className="bg-gray-300 text-gray-700 font-semibold px-4 py-2 rounded-lg hover:bg-gray-400"
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default TaskModal;
