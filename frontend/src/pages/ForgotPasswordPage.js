import React from "react";
import ForgotPasswordForm from "../components/auth/ForgotPasswordForm";
import Header from "../../src/components/Header";
const ForgotPasswordPage = () => {
  return (
    <div>
      <Header />
      <div className="w-full max-w-md mx-auto">
        <ForgotPasswordForm />
      </div>
    </div>
  );
};

export default ForgotPasswordPage;
