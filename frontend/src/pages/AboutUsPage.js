import React from "react";
import Header from "../../src/components/Header";
import AboutUs from "../components/AboutUs";
const AboutUsPage = () => {
  return (
    <div>
      <Header />
      <AboutUs />
    </div>
  );
};

export default AboutUsPage;
