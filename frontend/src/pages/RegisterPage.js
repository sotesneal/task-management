import React from 'react';
import RegistrationForm from '../components/auth/registerForm';
import Header from '../../src/components/Header';
const RegisterPage = () => {
    return (
        <div>
            <Header />
            <div className="w-full max-w-md mx-auto">
                <RegistrationForm />
            </div>
        </div>
    );
};

export default RegisterPage;
