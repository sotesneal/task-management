import React from 'react';
import ResetPasswordForm from '../components/auth/ResetPasswordForm';
import Header from '../../src/components/Header';
const ForgotPasswordPage = () => {
    return (
        <div>
            <Header />
            <div className="w-full max-w-md mx-auto">
                <ResetPasswordForm />
            </div>
        </div>
    );
};

export default ForgotPasswordPage
