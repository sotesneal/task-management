import React from "react";
import LoginForm from "../components/auth/loginForm";
import Header from "../../src/components/Header";
const LoginPage = () => {
  return (
    <div>
      <Header />
      <div className="w-full max-w-md mx-auto">
        <LoginForm />
      </div>
    </div>
  );
};

export default LoginPage;
