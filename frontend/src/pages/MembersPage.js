import React from "react";

const MembersPage = () => {
  // Replace with your actual team members' data
  const teamMembers = [
    { id: 1, name: "John Doe", role: "Web Developer" },
    { id: 2, name: "Jane Smith", role: "UI/UX Designer" },
    { id: 3, name: "Alice Johnson", role: "Product Manager" },
  ];

  return (
    <div className="flex flex-col items-center justify-center h-full">
      <h1 className="text-3xl font-semibold mb-6">Team Members</h1>
      <div className="flex flex-wrap justify-center">
        {teamMembers.map((member) => (
          <div
            key={member.id}
            className="bg-white p-4 m-2 rounded-md shadow-md w-64"
          >
            <h2 className="text-lg font-semibold mb-2">{member.name}</h2>
            <p className="text-sm text-gray-600">{member.role}</p>
            {/* Add more member-related information or actions here */}
          </div>
        ))}
      </div>
    </div>
  );
};

export default MembersPage;
