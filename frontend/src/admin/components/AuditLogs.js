import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';

const formatDueDate = (timestamp) => {
  

  const date = new Date(timestamp);
  const month = date.toLocaleString(undefined, { month: 'long' });
  const day = date.toLocaleString(undefined, { day: '2-digit' });
  const year = date.toLocaleString(undefined, { year: 'numeric' });

  return `${month} ${day}, ${year}`;
};

const excludedKeys = ["Priority", "Status", "CreatorUserId", "DueDate"]; // Keys to exclude

const formatKeyValuePairs = (jsonString) => {
  try {
    const obj = JSON.parse(jsonString);
    // Filter out keys that should be excluded
    const filteredObj = Object.keys(obj)
      .filter((key) => !excludedKeys.includes(key))
      .reduce((acc, key) => {
        acc[key] = obj[key];
        return acc;
      }, {});
    // Format the filtered JSON data as key-value pairs
    return Object.keys(filteredObj)
      .map((key) => `${key} : ${filteredObj[key]}`)
      .join('\n');
  } catch (error) {
    return jsonString; // Return the original string if it's not valid JSON
  }
};

const AuditLogs = ({ auditLogs }) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [pageNumber, setPageNumber] = useState(0);
  const itemsPerPage = 4; // Set the number of items per page

  // Function to filter auditLogs based on the search term
  const filterLogs = (logs) => {
    return logs.filter((log) =>
      JSON.stringify(log).toLowerCase().includes(searchTerm.toLowerCase())
    );
  };

  useEffect(() => {
    setPageNumber(0); // Reset page number when the search term changes
  }, [searchTerm]);

  // Calculate the total number of pages
  const pageCount = Math.ceil(filterLogs(auditLogs).length / itemsPerPage);

  // Function to handle page change
  const handlePageChange = ({ selected }) => {
    setPageNumber(selected);
  };

  // Calculate the range of logs to display on the current page
  const startIndex = pageNumber * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentLogs = filterLogs(auditLogs).slice(startIndex, endIndex);

  // Function to convert data to CSV format
  const convertToJSON = (data) => {
    const jsonData = JSON.stringify(data, null, 2); // Format JSON data with 2 spaces for indentation
  
    const blob = new Blob([jsonData], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
  
    const a = document.createElement('a');
    a.href = url;
    a.download = 'audit_logs.json';
    a.click();
    URL.revokeObjectURL(url);
  };
  

  // Function to download all data
  const downloadAllData = () => {
    convertToJSON(auditLogs); // Pass all audit logs data to the convertToCSV function
  };

  return (
    <div className="bg-white shadow-md rounded-lg bg-gradient-to-tl from-slate-50 to-white rounded-[32px] shadow">
      <div className="p-4">
        <input
          type="text"
          placeholder="Search..."
          className="w-full border rounded-md px-3 py-2 focus:outline-none focus:ring focus:border-blue-300"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </div>
      <table className="w-full table-auto">
        <thead>
          <tr className="bg-gray-200">
            <th className="px-4 py-3 text-left">Time</th>
            <th className="px-4 py-3 text-left">Action</th>
            <th className="px-4 py-3 text-left">Username</th>
            <th className="px-4 py-3 text-left">Details (Old Values)</th>
            <th className="px-4 py-3 text-left">Details (New Values)</th>
          </tr>
        </thead>
        <tbody>
          {currentLogs.map((log) => (
            <tr key={log.id} className="border-b border-gray-300">
              <td className="px-4 text-sm py-3">{formatDueDate(log.timestamp)}</td>
              <td className="px-4 text-sm py-3">{log.action}</td>
              <td className="px-4 text-sm py-3">{log.username}</td>
              <td className="px-4 text-sm py-3">
                <pre className=" text-sm whitespace-pre-wrap">{formatKeyValuePairs(log.oldValues)}</pre>
              </td>
              <td className="px-4 py-3">
                <pre className="text-sm whitespace-pre-wrap">{formatKeyValuePairs(log.newValues)}</pre>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <ReactPaginate
        previousLabel={'Previous'}
        nextLabel={'Next'}
        pageCount={pageCount}
        onPageChange={handlePageChange}
        containerClassName={'flex ml-2 mt-2 gap-2'}
        previousLinkClassName={'px-3 py-2 rounded-md bg-gray-200 text-sm font-medium text-gray-500 hover:bg-gray-300'}
        nextLinkClassName={'px-3 py-2 rounded-md bg-gray-200 text-sm font-medium text-gray-500 hover:bg-gray-300'}
        disabledClassName={'px-3 py-2 rounded-md bg-gray-200 text-sm font-medium text-gray-300 cursor-not-allowed'}
        activeClassName={'px-3 py-2 rounded-md bg-gray-200 text-sm font-medium text-gray-500'}
      />
      <div className="flex items-center justify-between p-4">
        <button
          className="px-3 py-2 rounded-md bg-blue-500 text-white hover:bg-blue-600"
          onClick={downloadAllData}
        >
          Download All Data (JSON)
        </button>
        <div className="text-gray-500">
          Page {pageNumber + 1} of {pageCount}
        </div>
      </div>
    </div>
  );
};

export default AuditLogs;
