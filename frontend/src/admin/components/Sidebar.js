import React from 'react';
import { useState, useEffect } from 'react';
import authApi from '../services/authService'; // Import your authentication service

const Sidebar = ({ handleGenerateAuditLogs }) => {
  const [activeLink, setActiveLink] = useState('Audit logs');

  const handleLogout = () => {
    // Clear user-related data from local storage
    localStorage.removeItem('token');
    localStorage.removeItem('userRoles');

    // Redirect to the login page (you can use your own routing logic here)
    window.location.href = '/';
  };

  // Use useEffect to trigger the generation of audit logs when the component is rendered
  useEffect(() => {
    handleGenerateAuditLogs();
  }, [handleGenerateAuditLogs]);

  return (
    <div className="px-4 py-6 bg-gray-200 flex flex-col h-full">
      <span className="grid h-10 w-32 place-content-center rounded-lg bg-gray-100 text-xl text-gray-600">
        Admin
      </span>
      <ul className="mt-6 space-y-1 flex-grow">
        <li>
          <a
            onClick={() => setActiveLink('Audit logs')}
            className={`block rounded-lg bg-gray-100 px-7 py-2 text-sm font-medium ${
              activeLink === 'Audit logs' ? 'text-gray-900 bg-blue-100' : 'text-gray-700'
            }`}
          >
            Audit logs
          </a>
        </li>
      </ul>
      <div className="mt-auto">
        <ul className="space-y-1">
          <li>
            <a
              onClick={handleLogout}
              className="block rounded-lg bg-gray-100 px-9 py-2 text-sm font-medium text-gray-700 hover:bg-red-600 hover:text-white cursor-pointer"
            >
              Logout
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
