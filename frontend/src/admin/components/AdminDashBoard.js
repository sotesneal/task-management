import React, { useState } from 'react';
import { fetchAuditLogs } from '../services/adminApi';
import Sidebar from './Sidebar';
import AuditLogs from './AuditLogs'; // Assuming you have a component for displaying audit logs

const AdminDashboard = () => {
  const [auditLogs, setAuditLogs] = useState([]);

  const handleGenerateAuditLogs = async () => {
    try {
      const tokenString = localStorage.getItem('token');
      const authToken = JSON.parse(tokenString);
      const logs = await fetchAuditLogs(authToken);
      setAuditLogs(logs);
    } catch (error) {
      console.error('Error fetching audit logs:', error);
    }
  };

  return (
    <div className="flex h-screen">
      <Sidebar handleGenerateAuditLogs={handleGenerateAuditLogs} />
      <div className="w-full bg-white">
        {/* Add other content, including AuditLogs component */}
        <AuditLogs auditLogs={auditLogs} />
        {/* Add other content */}
      </div>
    </div>
  );
};

export default AdminDashboard;
