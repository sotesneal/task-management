import React from 'react';

const Users = () => {
  return (
    <div className="flex flex-col justify-center items-center h-full">
      <h1 className="text-2xl font-semibold mb-4">Members Page</h1>
      <p>This is the content of the Members page.</p>
    </div>
  );
};

export default Users;
