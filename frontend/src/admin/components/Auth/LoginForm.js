import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { login } from '../../../admin/services/authService';

const LoginForm = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const navigate = useNavigate();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    setError('');

  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
    setError('');

  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const token = await login(email, password);
      if (token) {
        // Redirect to admin dashboard after successful login
        localStorage.setItem('token', JSON.stringify(token));
        navigate('/admin/dashboard');
      }
    } catch (error) {
      if (error.response && error.response.data) {
        // Check if the error message is an object, if so, extract the error message
        const errorMessage = typeof error.response.data === 'object'
          ? error.response.data.title || 'An error occurred while logging in.'
          : error.response.data;

        setError(errorMessage);
      } else {
        setError('An error occurred while logging in.');
      }
      console.error('Login error:', error);
    }
  };


  return (
    <div className="bg-transparent  rounded px-8 pt-6 pb-8 mb-4">
      <h2 className="text-4xl mb-8 text-center text-white font-semibold">Admin</h2>
      <form onSubmit={handleSubmit}>
        {error && <p className="text-red-500 mb-4">{error}</p>}
        <div className="mb-6">
          <label className="block text-gray-700 text-sm text-white font-bold mb-2" htmlFor="email">
            Email
          </label>
          <input
            className="shadow appearance-none bg-gray-700 border rounded w-full py-2 px-3 text-white leading-tight focus:outline-none focus:shadow-outline"
            id="email"
            type="email"
            placeholder="Email"
            required
            value={email}
            onChange={handleEmailChange}
          />
        </div>
        <div className="mb-6">
          <label className="block text-gray-700 text-white text-sm font-bold mb-2" htmlFor="password">
            Password
          </label>
          <input
            className="shadow appearance-none border bg-gray-700 rounded w-full py-2 px-3 text-white mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password"
            type="password"
            placeholder="Password"
            required
            value={password}
            onChange={handlePasswordChange}
          />
        </div>
        <div className="flex items-center justify-between">
          <button
            className="bg-blue-900 hover:bg-blue-700  text-white font-bold py-2 px-4 w-full rounded focus:outline-none focus:shadow-outline"
            type="submit"
          >
            Login
          </button>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;
