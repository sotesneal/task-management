import axios from 'axios';
import config from '../../config';

const authApi = axios.create({
  baseURL: `${config.BASE_URL}/api/account`, // Adjust the path based on your backend setup
});

export const login = async (email, password) => {
  try {
    const response = await authApi.post('/login', { email, password });
    const token = response.data.token;
    localStorage.setItem('token', token); // Store the token in local storage
    return token;
  } catch (error) {
    throw error;
  }
};

export const logout = () => {
  localStorage.removeItem('token'); // Remove the token from local storage
};

export const getToken = () => {
  return localStorage.getItem('token');
};

export const isAuthenticated = () => {
  const token = getToken();
  return !!token; // Check if a token exists
};

// Add more authentication-related functions as needed

export default authApi;
