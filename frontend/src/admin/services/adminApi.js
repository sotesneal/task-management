import axios from 'axios';

const API_BASE_URL = 'http://localhost:5228/api';


export const fetchAuditLogs = async (token) => {
  try{
    const response = await axios.get(`${API_BASE_URL}/auditlogs`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    return response.data;
  } catch (error) {
    console.error('Error fetching audit logs:', error);
  }
};
