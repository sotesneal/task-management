import React from 'react';
import LoginForm from '../components/Auth/LoginForm';

const AdminLoginPage = () => {
  return (
    <div className="bg-metal flex items-center justify-center min-h-screen bg-gray-100">
      <div className="w-full max-w-md">
        <LoginForm />
      </div>
    </div>
  );
};

export default AdminLoginPage;
