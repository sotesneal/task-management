import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { AuthService } from "../../services/authService";
import jwt_decode from "jwt-decode";
import { Link } from "react-router-dom";
import logo from "../../images/logo/logo-taskhub.png";
const LoginForm = () => {
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [state, setState] = useState({
    emailOrUsername: "",
    password: "",
    errors: {},
  });

  const handlePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };
  const navigate = useNavigate();

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setState((prevState) => ({
      ...prevState,
      [name]: value,
      errors: { ...prevState.errors, [name]: "" },
    }));
  };

  const validateForm = () => {
    let formErrors = {};

    if (!state.emailOrUsername) {
      formErrors.emailOrUsername = "Email or Username is required.";
    }

    if (!state.password) {
      formErrors.password = "Password is required.";
    }

    setState((prevState) => ({
      ...prevState,
      errors: formErrors,
    }));

    // If the formErrors object is empty, the form is valid.
    return Object.keys(formErrors).length === 0;
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    setIsLoading(true);

    try {
      const tokenResponse = await AuthService.login({
        emailOrUsername: state.emailOrUsername,
        password: state.password,
      });
      console.log(tokenResponse.errorCode);
      if (tokenResponse.errorCode === "UserNotFound") {
        setError("Couldn't find your account. Please register first.");
      } else if (tokenResponse.errorCode === "InvalidEmailOrPassword") {
        setError("Invalid email or password.");
      } else {
        const decodedToken = jwt_decode(tokenResponse);
        const userRoles = decodedToken.role;

        localStorage.setItem("token", JSON.stringify(tokenResponse));
        localStorage.setItem("userRoles", JSON.stringify(userRoles));

        console.log(tokenResponse);
        // Add a delay of at least 3 seconds
        await new Promise((resolve) => setTimeout(resolve, 3000));

        if (userRoles.includes("Administrator")) {
          navigate("/admin-dashboard");
        } else {
          navigate("/user-dashboard");
        }
      }
    } catch (error) {
      if (error.message.includes("Invalid token specified")) {
        setError("Invalid token specified");
      } else {
        setError(error.message);
      }
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <form
      onSubmit={handleLogin}
      class="mb-0 mt-14 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8 w-[430px] h-auto"
    >
      <img src={logo} alt="logo" className="w-3/4 mx-auto" />
      <p className="text-center  font-semibold text-indigo-600 mb-8 sm:text-2xl">
        Login to your account
      </p>
      {error && (
        <p className="text-red-500 mb-2 text-sm text-center">{error}</p>
      )}
      <div class="relative">
        <input
          type="text"
          id="emailOrUsername" // Update id and name to emailOrUsername
          name="emailOrUsername"
          aria-describedby="emailOrUsername"
          value={state.emailOrUsername}
          onChange={handleInputChange}
          className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
            state.errors.emailOrUsername ? "border-red-500" : "border-gray-200"
          } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
          placeholder=" "
        />
        {!state.errors.emailOrUsername && (
          <label
            for="emailOrUsername"
            class="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
          >
            Email or Username
          </label>
        )}
        {state.errors.emailOrUsername && (
          <p className="text-red-500 text-xs">{state.errors.emailOrUsername}</p>
        )}
      </div>

      <div class="relative">
        <input
          type={passwordVisible ? "text" : "password"}
          id="password"
          name="password"
          aria-describedby="password"
          value={state.password}
          onChange={handleInputChange}
          className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
            state.errors.password ? "border-red-500" : "border-gray-200"
          } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
          placeholder=" "
        />
        {!state.errors.password && (
          <label
            for="password"
            class="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2 peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
          >
            Password
          </label>
        )}
        <span class="absolute inset-y-0 end-0 grid place-content-center px-4">
          <button
            type="button"
            className="absolute right-3 top-4 text-gray-500"
            onClick={handlePasswordVisibility}
          >
            {passwordVisible ? (
              <i className="fas fa-eye-slash"></i>
            ) : (
              <i className="fas fa-eye"></i>
            )}
          </button>
        </span>
        {state.errors.password && (
          <p className="text-red-500 text-xs">{state.errors.password}</p>
        )}
        <p className="text-right  text-xs text-gray-500"></p>
      </div>
      <p className="text-right text-sm text-gray-500">
        <Link to="/forgot-password" className="underline">
          Forgot password?
        </Link>
      </p>

      <button
        type="submit"
        className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white"
        disabled={isLoading}
      >
        {isLoading ? (
          <span>
            <i className="fas fa-spinner fa-spin"></i> Logging in...
          </span>
        ) : (
          <span>Login</span>
        )}
      </button>

      <p className="text-center text-sm text-gray-500">
        Don't have an account?{" "}
        <Link to="/register" className="underline">
          Sign up
        </Link>
      </p>
    </form>
  );
};

export default LoginForm;
