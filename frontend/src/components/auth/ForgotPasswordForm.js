import React, { useState, useEffect } from 'react';
import { AuthService } from '../../services/authService';
import { Link } from 'react-router-dom';
import logo from '../../images/logo/logo-taskhub.png'

const ForgotPasswordForm = () => {
    const [error, setError] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [message, setMessage] = useState(''); 
    const [state, setState] = useState({
    email: '',
    errors: {}
  });
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    
    setState(prevState => ({
      ...prevState,
      [name]: value,
      errors: { ...prevState.errors, [name]: '' }
      
    }));
  };

  const validateForm = () => {
    let formErrors = {};

    if (!state.email) {
        formErrors.email = "Email is required.";
      } else if (!/\S+@\S+\.\S+/.test(state.email)) {
        formErrors.email = "Invalid email format.";
      }

    setState(prevState => ({
      ...prevState,
      errors: formErrors
    }));

    // If the formErrors object is empty, the form is valid.
    return Object.keys(formErrors).length === 0;
  };
  useEffect(() => {
    let hideMessageTimeout;

    // Check if there is a message to hide
    if (message) {
        // Set a timeout to hide the message after 4 seconds (4000 milliseconds)
        hideMessageTimeout = setTimeout(() => {
            setMessage('');
        }, 9000);
    }

    // Clean up the timeout when the component unmounts
    return () => {
        clearTimeout(hideMessageTimeout);
    };
}, [message]);
  const handleForgotPassword = async (e) => {
    e.preventDefault();
    if (!validateForm()) {
        return;
      }
    setIsLoading(true);

    try {
      // Send a reset password request to the backend
      await AuthService.forgotPassword({ email: state.email });
      setMessage('If an account with this email exists, a password reset email will be sent.');
      setError(''); 
    } catch (error) {
      setMessage('');
      setError('An error occurred. Please try again later.');
    } finally {
      setIsLoading(false);
    }
  };

  return (
        <form onSubmit={handleForgotPassword} class="mb-0 mt-10 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8 w-[430px] h-auto">
          <img src={logo}  alt="logo" className="w-3/4  mx-auto" />
          <p className="text-left font-semibold text-indigo-600  sm:text-2xl">Reset Password</p>
          {message && (
                <div className="bg-green-100 p-4 rounded-lg mb-4">
                    <p className='text-gray-400' style={{ color: 'green', fontWeight: 'bold' }}>{message}</p>
                </div>
            )}
          <p className='text-sm'>To reset your password, please provide the email address associated with your account. We will send you a password reset link to this email address.</p>
          <div class="relative">
          <input
                type="text"
                id="email"
                name="email"
                aria-describedby="email"
                value={state.email}
                onChange={handleInputChange}
                className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
                    state.errors.email ? 'border-red-500' : 'border-gray-200'
                } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
                placeholder=" "
            />
            {!state.errors.email && <label for="email" class="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Email</label>}
            {state.errors.email && <p className="text-red-500 text-xs">{state.errors.email}</p>}
          </div>

        
          <button
            type="submit"
            className={`block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white ${
                isLoading ? 'cursor-not-allowed opacity-50' : ''
            }`}
            disabled={isLoading}
            >
            {isLoading ? (
                <span>
                <i className="fas fa-spinner fa-spin"></i> Sending...
                </span>
            ) : (
                <span>Send Reset Email</span>
            )}
            </button>
            <div className="text-center">
                <Link to="/" className="text-indigo-600 hover:text-indigo-500 hover:underline">
                    Back to login
                </Link>
            </div>
         </form>
     
  );
};

export default ForgotPasswordForm;
