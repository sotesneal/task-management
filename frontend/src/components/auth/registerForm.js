import React, { useState, useEffect } from 'react';
import { AuthService } from '../../services/authService';
import { useNavigate, Link } from 'react-router-dom';
import logo from '../../images/logo/logo-taskhub.png'

const RegistrationForm = () => {
  const [formData, setFormData] = useState({
    email: '',
    username: '',
    password: '',
    firstName: '',
    lastName: '',
  });

  const [passwordVisible, setPasswordVisible] = useState(false);
  const [confirmPasswordVisible, setConfirmPasswordVisible] = useState(false); // Added state for confirm password visibility
  const [errors, setErrors] = useState({});
  const [generalError, setGeneralError] = useState('');
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();


  
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  
    // Real-time length validation while typing
    if (name === 'username' || name === 'firstName' || name === 'lastName') {
      const minLength = name === 'username' ? 4 : 2;
      const maxLength = name === 'username' ? 20 : 30;
      
      if (name === 'username') {
        // Add a validation to check for spaces in the username
        const spacePattern = /\s/;
        if (spacePattern.test(value)) {
          setErrors((prevErrors) => ({
            ...prevErrors,
            [name]: 'Username cannot contain spaces.',
          }));
        } else if (value.length < minLength || value.length > maxLength) {
          setErrors((prevErrors) => ({
            ...prevErrors,
            [name]: 'Username must be between 4 and 20 characters.',
          }));
        } else {
          setErrors((prevErrors) => ({
            ...prevErrors,
            [name]: '', // Clear the error if the input is valid
          }));
        }
      } else if (name === 'firstName' || name === 'lastName') {
        // Add a validation to check for non-alphabetical characters (including spacing)
        const nonAlphabeticalPattern = /[^a-zA-Z\s]/;
        if (nonAlphabeticalPattern.test(value)) {
          setErrors((prevErrors) => ({
            ...prevErrors,
            [name]: `${name.charAt(0).toUpperCase() + name.slice(1).replace(/([A-Z])/g, ' $1')} cannot contain non-alphabetical characters.`,
            
          }));
        } else if (value.length < minLength || value.length > maxLength) {
          setErrors((prevErrors) => ({
            ...prevErrors,
            [name]: `${name.charAt(0).toUpperCase() + name.slice(1).replace(/([A-Z])/g, ' $1')} must be between 2 and 30 characters.`,
          }));
        } else {
          setErrors((prevErrors) => ({
            ...prevErrors,
            [name]: '', // Clear the error if the input is valid
          }));
        }
      } else {
        setErrors((prevErrors) => ({
          ...prevErrors,
          [name]: '', // Clear the error if the length is within bounds
        }));
      }
    }
  
    // Real-time email format validation while typing
    if (name === 'email') {
      if (!/\S+@\S+\.\S+/.test(value)) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          [name]: 'Invalid email format.',
        }));
      } else {
        setErrors((prevErrors) => ({
          ...prevErrors,
          [name]: '', // Clear the error if the email format is valid
        }));
      }
    }
  
    // Real-time password validation while typing
    if (name === 'password') {
      const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&*!])[A-Za-z\d@#$%^&*!]{8,}$/;
      if (!passwordPattern.test(value)) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          [name]: 'Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character. (Minimum 8 characters)',
        }));
       } else {
        setErrors((prevErrors) => ({
          ...prevErrors,
          [name]: '', // Clear the error if the password format is valid
        }));
      }
    }
  };
  
  
  
  

  const handlePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };
  const handleConfirmPasswordVisibility = () => {
    setConfirmPasswordVisible(!confirmPasswordVisible);
  };

  useEffect(() => {
    if (registrationSuccess) {
      // Automatically redirect to login after 2 seconds
      const redirectTimer = setTimeout(() => {
        navigate('/');
      }, 4000);

      return () => {
        clearTimeout(redirectTimer);
      };
    }
  }, [registrationSuccess, navigate]);

  const handleRegistration = async (e) => {
    e.preventDefault();

    // Disable the submit button while processing
    setLoading(true);

    const validationErrors = validateForm();
    if (Object.keys(validationErrors).length === 0) {
      try {
        // Simulate a delay using setTimeout
        setTimeout(async () => {
          const response = await AuthService.register(formData);
          console.log(response.errors);
          if (Object.keys(response).length > 0) {
            // Handle validation errors returned by the API
            setErrors(response);
          } else {
            // Clear any previous general error
            setGeneralError('');
            setRegistrationSuccess(true);
          }

          // Re-enable the submit button after processing
          setLoading(false);
        }, 1000); // Delay of 1 second (1000 milliseconds)
      } catch (error) {
        setGeneralError('An error occurred while registering.');
        // Re-enable the submit button after processing
        setLoading(false);
      }
    } else {
      setErrors(validationErrors);
      // Re-enable the submit button after processing
      setLoading(false);
    }
  };

  const validateForm = () => {
    const validationErrors = {};
  
    const fields = ['email', 'username', 'password', 'firstName', 'lastName'];
    //user name must be between 4 and 20 characters
    if (formData.username.length < 4 || formData.username.length > 20) {
      validationErrors.username = ['Username must be between 4 and 20 characters.'];
    }
    //first name must be between 2 and 30 characters
    if (formData.firstName.length < 2 || formData.firstName.length > 30) {
      validationErrors.firstName = ['First name must be between 2 and 30 characters.'];
    }
    //last name must be between 2 and 30 characters
    if (formData.lastName.length < 2 || formData.lastName.length > 30) {
      validationErrors.lastName = ['Last name must be between 2 and 30 characters.'];
    }
   
    // Confirm password validation
    if (formData.password !== formData.confirmPassword) {
      validationErrors.confirmPassword = ['Passwords do not match.'];
    }
  
    fields.forEach((field) => {
      if (!formData[field]) {
        // Use a space in the field name for the error message
        validationErrors[field] = [`${field.charAt(0).toUpperCase()}${field.slice(1).replace(/([A-Z])/g, ' $1')} is required.`];
      }
    });
  
    if (formData.email && !/\S+@\S+\.\S+/.test(formData.email)) {
      validationErrors.email = ['Invalid email format.'];
    }
  
    // Password validation
    if (formData.password) {
      const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
      if (!passwordPattern.test(formData.password)) {
        // Use a space in the field name for the error message
        validationErrors.password = ['Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character.'];
      }
    }
  
    // Username validation
    if (formData.username) {
      const spacePattern = /\s/;
      if (spacePattern.test(formData.username)) {
        validationErrors.username = ['Username cannot contain spaces.'];
      }
    }
  
    // First Name and Last Name validation
    ['firstName', 'lastName'].forEach((field) => {
      const nonAlphabeticalPattern = /[^a-zA-Z\s]/;
      if (formData[field] && nonAlphabeticalPattern.test(formData[field])) {
        validationErrors[field] = [`${field.charAt(0).toUpperCase()}${field.slice(1).replace(/([A-Z])/g, ' $1')} cannot contain non-alphabetical characters.`];
      }
    });
  
    return validationErrors;
  };
  
  
  

  const renderInput = (name, label) => (
    <div className={`relative ${errors[name] ? 'mb-2' : 'mb-6'}`}>
      <input
        type={name === 'password' || name === 'confirmPassword' ? (passwordVisible ? 'text' : 'password') : 'text'} // Set the correct input type
        id={name}
        name={name}
        onChange={handleInputChange}
        className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
          errors[name] ? 'border-red-500' : 'border-gray-200'
        } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
        placeholder=" "
      />
      {!errors[name] && (
        <label
          htmlFor={name}
          className="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
        >
          {label}
        </label>
      )}
      {errors[name] && (
        <p className="text-red-500 text-xs">{errors[name]}</p>
      )}
      {(name === 'password' || name === 'confirmPassword') && (
        <button
          type="button"
          className="absolute right-3 top-4 text-gray-500"
          onClick={name === 'password' ? handlePasswordVisibility : handleConfirmPasswordVisibility}
        >
          {name === 'password' ? (
            passwordVisible ? (
              <i className="fas fa-eye-slash"></i>
            ) : (
              <i className="fas fa-eye"></i>
            )
          ) : null}
        </button>
      )}
    </div>
  );
  
  
  

  return (
    <form
      onSubmit={handleRegistration}
      className="mb-0 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8 w-[430px] h-auto"
    >
      <img src={logo} alt="logo" className="w-3/4 mx-auto" />
      <p className="text-center font-semibold text-indigo-600 sm:text-2xl">Register an account</p>
      {errors.generalError && <p className="text-red-500 text-sm text-center mb-4">{errors.generalError}</p>}
      {registrationSuccess && (
        <div className="bg-green-100 p-4 rounded-lg mb-4">
          <p className='text-gray-400' style={{ color: 'green', fontWeight: 'bold' }}>Registration successful! You can now sign in.</p>
        </div>
      )}
      {renderInput('username', 'Username')}
      {renderInput('firstName', 'First Name')}
      {renderInput('lastName', 'Last Name')}
      {renderInput('email', 'Email')}
      {renderInput('password', 'Password')}
      {renderInput('confirmPassword', 'Confirm Password')}


      <button
        type="submit"
        className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white relative"
      >
        {loading ? (
          <span>
            <i className="fas fa-spinner fa-spin"></i> Registering...
          </span>
        ) : (
          <span>Register</span>
        )}
      </button>
      <p className="text-center text-sm  text-gray-500">
        Already have an account? <Link to="/" className="underline">Log in</Link>
      </p>
    </form>
  );
};

export default RegistrationForm;
