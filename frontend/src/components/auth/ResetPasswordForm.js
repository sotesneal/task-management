import React, { useState, useEffect } from 'react';
import { AuthService } from '../../services/authService';
import logo from '../../images/logo/logo-taskhub.png';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

const ResetPasswordForm = () => {
  const { userId, code } = useParams();

  const [state, setState] = useState({
    password: '',
    confirmPassword: '',
    errors: {},
  });

  const [passwordVisible, setPasswordVisible] = useState(false);
  const [resetSuccess, setResetSuccess] = useState(false);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setState((prevData) => ({
      ...prevData,
      [name]: value,
      errors: { ...prevData.errors, [name]: '' }, 
    }));
  };
  
  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };
  const [passwordValid, setPasswordValid] = useState(true);

  // Add a useEffect to perform real-time password validation
  useEffect(() => {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (state.password && !passwordRegex.test(state.password)) {
      setPasswordValid(false);
    } else {
      setPasswordValid(true);
    }
  }, [state.password]);

  const handleResetPassword = async (e) => {
    e.preventDefault();
  
    setLoading(true);
  
    const validationErrors = validateForm();
    if (Object.keys(validationErrors).length === 0) {
      try {
        const response = await AuthService.resetPassword({
          userId: userId,
          token: code,
          newPassword: state.password,
        });
        console.log(response);
        if (response === 'Password reset successful.') {
          // Add a delay before setting reset success and redirecting
          setResetSuccess(true);
          setTimeout(() => {
            navigate('/');
          }, 4000); // Delay for 4 seconds (adjust as needed)
        } else if (response === 'Invalid or expired token.') {
          setState((prevState) => ({
            ...prevState,
            errors: { server: 'Password Reset Link Expired or Invalid' },
          }));
        } else if (response.status === 400) {
          setState((prevState) => ({
            ...prevState,
            errors: { server: 'Password reset failed. Please try again later.' },
          }));
        }
  
        setLoading(false);
      } catch (error) {
        console.error(error);
        setLoading(false);
        setState((prevState) => ({
          ...prevState,
          errors: { server: 'An error occurred. Please try again later.' },
        }));
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        errors: validationErrors, // Set validation errors
      }));
      setLoading(false);
    }
  };
  
  const validateForm = () => {
    const validationErrors = {};
  
    // Confirm password validation
    if (state.password !== state.confirmPassword) {
      validationErrors.confirmPassword = 'Passwords do not match.';
    }
    // Password is required
    if (!state.password) {
      validationErrors.password = 'Password is required.';
    }
    if (!state.confirmPassword) {
      validationErrors.confirmPassword = 'Confirm Password is required.';
    }
    // Updated passwordRegex to match your requirements
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (!passwordRegex.test(state.password)) {
      validationErrors.password = 'Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character, and be at least 8 characters long.';
    }
  
    // Add more validation rules as needed
  
    return validationErrors;
  };

  return (
    <form
      onSubmit={handleResetPassword}
      className="mt-10 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8 w-[430px] h-auto"
    >
      <img src={logo} alt="logo" className="w-3/4 mx-auto" />
      <p className="text-center font-semibold text-indigo-600 sm:text-2xl">Reset Your Password</p>
      {resetSuccess && (
        <div className="bg-green-100 p-3 rounded-lg mb-4">
          <p className='text-gray-400' style={{ color: 'green', fontWeight: 'bold' }}>Password reset successful! You can now sign in.</p>
        </div>
      )}
      {state.errors.server && <p className="text-red-500 text-sm text-center mb-4">{state.errors.server}</p>}
      <div className={`relative ${state.errors.password ? 'mb-2' : 'mb-6'}`}>
        <input
          type={passwordVisible ? 'text' : 'password'}
          id="password"
          name="password"
          onChange={handleInputChange}
          className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
            state.errors.password ? 'border-red-500' : 'border-gray-200'
          } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
          placeholder="New Password"
        />
        {!state.errors.password && (
          <label
            htmlFor="password"
            className="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
          >
            New Password
          </label>
        )}
        {state.errors.password && <p className="text-red-500 text-xs">{state.errors.password}</p>}
        <button
          type="button"
          className="absolute right-3 top-4 text-gray-500"
          onClick={togglePasswordVisibility}
        >
          {passwordVisible ? (
            <i className="fas fa-eye-slash"></i>
          ) : (
            <i className="fas fa-eye"></i>
          )}
        </button>
      </div>
      <div className={`relative ${state.errors.confirmPassword ? 'mb-2' : 'mb-6'}`}>
        <input
          type={passwordVisible ? 'text' : 'password'}
          id="confirmPassword"
          name="confirmPassword"
          onChange={handleInputChange}
          className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
            state.errors.confirmPassword ? 'border-red-500' : 'border-gray-200'
          } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
          placeholder="Confirm New Password"
        />
        {!state.errors.confirmPassword && (
          <label
            htmlFor="confirmPassword"
            className="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
          >
            Confirm New Password
          </label>
        )}
        {state.errors.confirmPassword && (
          <p className="text-red-500 text-xs">{state.errors.confirmPassword}</p>
        )}
      </div>
      <button
        type="submit"
        className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white relative"
      >
        {loading ? (
          <span>
            <i className="fas fa-spinner fa-spin"></i> Resetting...
          </span>
        ) : (
          <span>Reset Password</span>
        )}
      </button>
    </form>
  );
};

export default ResetPasswordForm;
