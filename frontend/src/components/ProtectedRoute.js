// ProtectedRoute.js

import React from "react";
import { Route, Navigate } from "react-router-dom";

const ProtectedRoute = ({
  component: Component,
  isAuthenticated,
  path,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated ? <Navigate to="/dashboard" /> : <Component {...props} />
    }
  />
);

export default ProtectedRoute;
