import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { AuthService } from "../services/authService";
import ArchivedTasksSidebar from "./ArchivedTasksSidebar"; // Import ArchivedTasksSidebar
import taskService from "../services/taskService";

// Define a constant background color
const defaultBackgroundColor = "#007BFF"; // You can change this to any color you prefer

// Function to get capitalized user's initials
function getCapitalizedUserInitials(user) {
  if (user && user.firstname && user.lastname) {
    const initials =
      user.firstname[0].toUpperCase() + user.lastname[0].toUpperCase();
    return initials;
  }
  return "NS"; // Default to 'NS' if user information is not available
}

// Function to capitalize only the first letter of a string
function capitalizeFirstLetter(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

const UserDashboardHeader = ({ currentColumn, openModalWithTask,updateTaskOnUnarchive }) => {
  const [showDropdown, setShowDropdown] = useState(false);
  const [userInfo, setUserInfo] = useState(null);
  const [showArchivedSidebar, setShowArchivedSidebar] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]); // State for search results
  const headerRef = useRef(null);
  const navigate = useNavigate();

  useEffect(() => {
    fetchUserInfo();
  }, []);

  const fetchUserInfo = async () => {
    try {
      const token = localStorage.getItem("token");
      console.log(token);
      const user = await AuthService.getUserInfo(JSON.parse(token));
      setUserInfo(user);
    } catch (error) {
      console.error("Error fetching user info:", error);
    }
  };
  const closeArchivedSidebar = () => {
    setShowArchivedSidebar(false);
  };
  const handleDocumentClick = (event) => {
    if (
      headerRef.current &&
      !headerRef.current.contains(event.target) &&
      !showArchivedSidebar
    ) {
      // Click occurred outside the header and sidebar is not open, close the sidebar
      closeArchivedSidebar();
    }
  };
  const handleProfileClick = () => {
    // Navigate to the user profile page
  navigate("/profile");
  };
  useEffect(() => {
    // Add a click event listener to the document
    document.addEventListener("click", handleDocumentClick);

    // Cleanup the event listener when the component unmounts
    return () => {
      document.removeEventListener("click", handleDocumentClick);
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ ]);
  const handleLogout = () => {
    // Clear user-related data from local storage
    localStorage.removeItem("token");
    localStorage.removeItem("userRoles");

    // Redirect to the login page (you can use your own routing logic here)
    window.location.href = "/";
  };

  const toggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };
  const tokenString = localStorage.getItem("token");
  const authToken = JSON.parse(tokenString);
  const handleSearchInputChange = (event) => {
    const query = event.target.value;
    setSearchQuery(query);

    // Fetch search results when the query is not empty
    if (query.trim() !== "") {
      taskService
        .searchTasks(authToken, query)
        .then((results) => {
          // Filter and map the results to strings and then filter based on query
          const filteredResults = results.filter((result) =>
            result.title.toLowerCase().startsWith(query.toLowerCase())
          );

          setSearchResults(filteredResults);
        })
        .catch((error) => {
          console.error("Error fetching search results:", error);
        });
    } else {
      // Clear search results if the search query is empty
      setSearchResults([]);
    }
  };

  const getStatusLabel = (status) => {
    switch (status) {
      case 0:
        return "Todo";
      case 1:
        return "In Progress";
      case 2:
        return "Done";
      default:
        return "";
    }
  };

  const handleArchivedTasksClick = () => {
    console.log("Clicked Archived items button"); // Check if the button click is registered
    setShowArchivedSidebar(!showArchivedSidebar);
    console.log("showArchivedSidebar:", showArchivedSidebar); // Check the state
  };

  return (
    <header className="bg-metalic  mb-4 " ref={headerRef}>
      <div className="max-w-screen-2xl  mx-auto px-4 sm:px-6 md:px-8 h-full">
        <div className="flex justify-between items-center h-full">
          <div className="flex items-center"></div>

          <div className="flex items-center space-x-3 md:space-x-8">
            <div className="flex  items-center">
              <input
                type="text"
                placeholder="Search..."
                className="px-3 py-2 rounded-md border focus:outline-none focus:ring-2 focus:ring-indigo-400"
                style={{ width: "300px" }}
                value={searchQuery}
                onChange={handleSearchInputChange}
              />
              {searchQuery && (
                <div className="search-dropdown absolute top-20 w-80 bg-white z-10 rounded-md shadow-md">
                  {searchResults.map((result) => (
                    <div
                      key={result.id}
                      className="search-result p-2 hover:bg-indigo-100 cursor-pointer"
                      onClick={() => {
                        console.log("Result Status:", result.status); // Log result.status
                        console.log("Current Column:", currentColumn); // Log currentColumn
                        openModalWithTask(result, result.status);
                      }}
                    >
                      <div className="text-sm font-semibold">
                        {result.title}
                      </div>
                      <div className="text-xs mt-1 text-gray-500">
                        In list: {getStatusLabel(result.status)}
                      </div>
                    </div>
                  ))}
                </div>
              )}
            </div>

            <div className="relative z-10">
              <button
                className="text-gray-600 hover:text-indigo-600"
                onClick={toggleDropdown}
              >
                {userInfo && userInfo.profilePictureUrl ? (
                  <img
                    src={`http://localhost:5228/${userInfo.profilePictureUrl}`}
                    alt="Profile"
                    className="w-8 h-8 rounded-full"
                  />
                ) : (
                  <div
                    className="w-8 h-8 rounded-full flex items-center justify-center"
                    style={{
                      backgroundColor: defaultBackgroundColor,
                      color: "white",
                    }}
                  >
                    {getCapitalizedUserInitials(userInfo)}
                  </div>
                )}
              </button>
              {showDropdown && (
                <div className="absolute right-0 p-2 mt-2 w-60 bg-white rounded-md shadow-md">
                  <h1 className="text-sm text-gray-600 font-semibold py-1 px-2">
                    ACCOUNT
                  </h1>
                  <ul className="py-1">
                    <li className="border-b">
                      <div className="flex items-center p-3">
                        {userInfo && userInfo.profilePictureUrl ? (
                          <img
                            src={`http://localhost:5228/${userInfo.profilePictureUrl}`}
                            alt="Profile"
                            className="w-10 h-10 rounded-full"
                          />
                        ) : (
                          <div
                            className="w-10 h-10 rounded-full flex items-center justify-center"
                            style={{
                              backgroundColor: defaultBackgroundColor,
                              color: "white",
                            }}
                          >
                            {getCapitalizedUserInitials(userInfo)}
                          </div>
                        )}
                        <div className="ml-3">
                          <p className="text-sm text-gray-700">
                            {userInfo
                              ? `${capitalizeFirstLetter(
                                  userInfo.firstname
                                )} ${capitalizeFirstLetter(userInfo.lastname)}`
                              : "NS"}
                          </p>
                          <p className="text-xs text-gray-500">
                            {userInfo && `${userInfo.email}`}
                          </p>
                        </div>
                      </div>
                    </li>
                    <li>
                      <button
                        className="w-full py-2 px-2 text-sm text-gray-700 hover:bg-indigo-100 text-left"
                        onClick={handleProfileClick}
                      >
                        Profile
                      </button>
                    </li>
                    <li>
                      <button
                        className="w-full py-2 px-2 text-sm text-gray-700 hover:bg-indigo-100 text-left"
                        onClick={handleArchivedTasksClick}
                      >
                        Archived items
                      </button>
                    </li>
                    <li>
                      <button
                        className="w-full py-2 px-2 text-sm text-gray-700 hover:bg-indigo-100 text-left"
                        onClick={handleLogout}
                      >
                        Log out
                      </button>
                    </li>
                  </ul>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
      {showArchivedSidebar && (
        <ArchivedTasksSidebar
          token={authToken} 
          isOpen={showArchivedSidebar}
          onClose={() => setShowArchivedSidebar(false)}
          updateTaskOnUnarchive={updateTaskOnUnarchive}
        />
      )}
    </header>
  );
};

export default UserDashboardHeader;
