import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../images/logo/logo-taskhub.png'

const Header = () => {
    return (
        <header className="bg-indigo-600 py-4">
            <div className="max-w-screen-xl mx-auto flex justify-between items-center px-4">
                <div></div>
                <nav className="space-x-4">
                    <Link to="/" className="text-white hover:underline">Home</Link>
                    <Link to="/about" className="text-white hover:underline">About Us</Link>
                   
                </nav>
            </div>
        </header>
    );
};

export default Header;
