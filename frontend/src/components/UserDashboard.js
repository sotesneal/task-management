import React, { useState } from "react";
import Sidebar from "./Sidebar";
import TaskPage from "../pages/TaskPage";
import MembersPage from "../pages/MembersPage";

const UserDashboard = () => {
  const [activeContent, setActiveContent] = useState("board"); // Initialize active content state

  const renderActiveContent = () => {
    // Render content based on activeContent state
    if (activeContent === "board") {
      return  <TaskPage />; 
    } else if (activeContent === "members") {
      return <MembersPage/>
    }
    // Add more cases for other content
  };

  return (
    <div className="flex">
      <Sidebar setActiveContent={setActiveContent} activeContent={activeContent} />
      <div className="flex-1 p-6 bg-grayish">{renderActiveContent()}</div>
    </div>
  );
};

export default UserDashboard;
