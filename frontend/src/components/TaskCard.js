import React, { useState, useEffect } from "react";
import { Draggable } from "react-beautiful-dnd";
import { DotsVerticalIcon } from "@heroicons/react/solid";
import { toast } from "react-toastify";
import taskService from "../services/taskService";

const TaskCard = ({
  item,
  index,
  onRemove,
  columnId,
  toggleModal,
  editingTaskId,
  editingTaskTitle,
  onPinToggle,
  taskUnarchived,
  taskUnarchivedId,
}) => {
  const [isArchived, setIsArchived] = useState(() => {
    const archived = item.isArchived === true;
    return archived;
  });
  const [isDeleted, setIsDeleted] = useState(false);
  const [showMenu, setShowMenu] = useState(false);
  const [isHovered, setIsHovered] = useState(false);
  const [title, setTitle] = useState(item.title);
  const [isPinned, setIsPinned] = useState(() => {
    const pinned = localStorage.getItem(`pinned_${item.id}`);
    const initialIsPinned = pinned === "true";
    console.log("Initial isPinned:", initialIsPinned); // Add this log
    return initialIsPinned;
  });

  const handlePinToggle = async () => {
    try {
      const tokenString = localStorage.getItem("token");
      const authToken = JSON.parse(tokenString);

      // Toggle the pinned status
      const updatedIsPinned = !isPinned;

      // Make an API request to update the pinned status
      await taskService.updateTaskPinnedStatus(
        authToken,
        item.id,
        updatedIsPinned
      );

      toast.success(`Task ${updatedIsPinned ? "pinned" : "unpinned"}!`, {
        autoClose: 1000,
      });
      setShowMenu(false);
      setIsPinned(updatedIsPinned);
      console.log(`isPinned updated to: ${updatedIsPinned}`);
      onPinToggle(item.id, updatedIsPinned);
    } catch (error) {
      console.error("Error updating task pinned status:", error);
      // Handle errors and show an appropriate error message to the user
      toast.error("Error updating task pinned status.", { autoClose: 2000 });
    }
  };

  const toggleMenu = () => {
    setShowMenu(!showMenu);
  };

  // Function to handle mouse enter
  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  useEffect(() => {
     if(item.id === taskUnarchivedId){
      setIsArchived(taskUnarchived);
     }
    
  }, [taskUnarchived, taskUnarchivedId, item.id]);
  

  useEffect(() => {
    setTitle(editingTaskTitle);
   
  }, [editingTaskTitle, title]);
  // Function to handle mouse leave
  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const handleRemoveClick = async () => {
    setShowMenu(false);
    try {
      const confirmToastId = toast.info(
        <div className="bg-white p-2 rounded-lg">
          <div className="text-lg font-semibold mb-2">Delete Task</div>
          <div className="text-gray-700 text-sm mb-4">
            Are you sure you want to delete this task?
          </div>
          <div className="flex justify-center">
            <button
              className="bg-red-500 text-white font-semibold px-4 py-2 rounded-lg mr-2 hover:bg-red-600"
              onClick={async () => {
                toast.dismiss(confirmToastId);
                const tokenString = localStorage.getItem("token");
                const authToken = JSON.parse(tokenString);
                await taskService.removeTask(authToken, item.id);
                setIsDeleted(true);
                onRemove(item.id);
                toast.success("Task deleted!", {
                  autoClose: 2000,
                });
              }}
            >
              OK
            </button>
            <button
              className="bg-gray-300 text-gray-700 font-semibold px-4 py-2 rounded-lg hover:bg-gray-400"
              onClick={() => {
                toast.dismiss(confirmToastId);
              }}
            >
              Cancel
            </button>
          </div>
        </div>,
        {
          autoClose: false,
          position: toast.POSITION.TOP_CENTER,
          closeButton: false,
        }
      );
    } catch (error) {
      console.error("Error removing task:", error);
      toast.error("Error deleting the card.", { autoClose: 2000 });
    }
  };

  const handleArchiveClick = async () => {
    try {
      const tokenString = localStorage.getItem("token");
      const authToken = JSON.parse(tokenString);
      await taskService.archiveTask(authToken, item.id);
      setIsArchived(true);
      toast.success("Task archived!", { autoClose: 1000 });
      setShowMenu(false);
      localStorage.setItem(`archived_${item.id}`, "false");
    } catch (error) {
      console.error("Error archiving task:", error);
      toast.error("Error archiving the card.", { autoClose: 2000 });
    }
  };

  const handleCardClick = (e) => {
    if (
      !e.target.classList.contains("verticalDotIcon") &&
      !e.target.classList.contains("delete-card") &&
      !e.target.classList.contains("archive-card") &&
      !e.target.classList.contains("pin-card")
    ) {
      // if (e.detail === 2) {
      //   toggleModal(item, columnId);
      // }
      toggleModal(item, columnId);
    }
  };

  // Define a CSS class to reduce contrast for "Done" tasks
  const doneCardClass = columnId === "done" ? "done-card" : "";

  const titleStyle =
    columnId === "done" ? { textDecoration: "line-through" } : {};

  if (isArchived || isDeleted) {
    return null;
  }
  const formatDate = (dateString) => {
    const options = { month: "short", day: "numeric" };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };
  const getPriorityLabel = (priority) => {
    switch (priority) {
      case 1:
        return (
          <span className="bg-green-300 text-green-700  px-1 py-1">Low</span>
        );
      case 2:
        return (
          <span className="bg-yellow-300 text-yellow-700 px-1 py-1">
            Medium
          </span>
        );
      case 3:
        return <span className="bg-red-300 text-red-700  px-1 py-1">High</span>;
      default:
        return "";
    }
  };

  // Function to check if the due date is happening 1 day before
  const isDueToday = (dueDate) => {
    if (
      !dueDate ||
      dueDate === "0001-01-01T00:00:00" ||
      dueDate === "0001-01-01 00:00:00.0000000"
    ) {
      return false;
    }

    const currentDate = new Date();
    const dueDateDate = new Date(dueDate);

    // Compare only the date part (ignore time)
    currentDate.setHours(0, 0, 0, 0);
    dueDateDate.setHours(0, 0, 0, 0);

    return currentDate.getTime() === dueDateDate.getTime();
  };

  const isDueSoon = (dueDate) => {
    if (!dueDate || dueDate === "0001-01-01T00:00:00") {
      return false;
    }

    const currentDate = new Date();
    const dueDateDate = new Date(dueDate);

    // Compare only the date part (ignore time)
    currentDate.setHours(0, 0, 0, 0);
    dueDateDate.setHours(0, 0, 0, 0);

    const oneDayBeforeDueDate = new Date(dueDateDate);
    oneDayBeforeDueDate.setDate(dueDateDate.getDate() - 1);

    return currentDate >= oneDayBeforeDueDate && currentDate < dueDateDate;
  };

  const isOverdue = (dueDate) => {
    if (!dueDate || dueDate === "0001-01-01T00:00:00") {
      return false;
    }

    const currentDate = new Date();
    const dueDateDate = new Date(dueDate);

    // Compare only the date part (ignore time)
    currentDate.setHours(0, 0, 0, 0);
    dueDateDate.setHours(0, 0, 0, 0);

    return currentDate >= dueDateDate;
  };

  const renderViewMode = () => {
    const isDueDateToday = isDueToday(item.dueDate);
    const isDueDateSoon = isDueSoon(item.dueDate);
    const isTaskOverdue = isOverdue(item.dueDate);

    const dueDateColorClass = isDueDateToday
      ? "text-red-600"
      : isTaskOverdue
      ? "text-red-400"
      : isDueDateSoon
      ? "text-yellow-500"
      : "text-gray-500";

    const dueDateText =
      isDueDateToday || isTaskOverdue || isDueDateSoon
        ? isDueDateToday
          ? "Due Today"
          : isTaskOverdue
          ? "Overdue"
          : "Due Soon"
        : "Due";

    return (
      <div>
        <div className="text-base font-semibold mb-1" style={titleStyle}>
          <div className="flex items-center">
            {" "}
            {/* Wrap the title and pin icon in a flex container */}
            {isPinned && (
              <span className="mr-2">📌</span> // Add some margin to separate the pin icon
            )}
            {editingTaskId === item.id ? editingTaskTitle : item.title}
          </div>
        </div>
        <div className="flex items-center">
          {" "}
          {/* Wrap priority and due date in a flex container */}
          {item.priority !== undefined && (
            <span className="text-xs">{getPriorityLabel(item.priority)}</span>
          )}
          {item.dueDate && item.dueDate !== "0001-01-01T00:00:00" && (
            <div className="text-xs">
              <span
                className={`inline-block p-1 ${dueDateColorClass} ${
                  isDueDateToday || isTaskOverdue
                    ? "bg-red-200"
                    : isDueDateSoon
                    ? "bg-yellow-200"
                    : ""
                }`}
              >
                {dueDateText}: {formatDate(item.dueDate)}
              </span>
            </div>
          )}
        </div>
        {isHovered && (
          <div className="absolute top-2 right-0">
            <DotsVerticalIcon
              className="absolute right-4 w-7 h-7 z-10 cursor-pointer text-gray-500 hover:text-gray-700 verticalDotIcon"
              onClick={toggleMenu}
              style={{
                border: "1px solid #ccc", // Add a border
                borderRadius: "50%", // Make it circular
                padding: "8px", // Increase padding for easier clicking
              }}
            />
          </div>
        )}
      </div>
    );
  };

  return (
    <Draggable
      key={String(item.id)}
      draggableId={String(item.id)}
      index={index}
    >
      {(provided) => (
        <div
          className={`${
            !isDeleted && !isArchived ? "animate-fade-in" : "animate-fade-out"
          } ${doneCardClass} flex flex-col justify-start items-start rounded-lg bg-white mt-2 border border-gray-300 shadow-md relative w-full py-5 px-6 bg-white shadow-md rounded-md transition transform hover:scale-105 hover:shadow-lg cursor-pointer select-none hover:border-blue-400`}
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
          onClick={handleCardClick}
        >
          <div className="flex items-center">{renderViewMode()}</div>
          {showMenu && (
            <div className="absolute top-1 z-10 right-9 w-35 bg-white rounded-lg shadow-md ">
              <div
                className="px-3 py-1 text-xs hover-bg-gray-100 cursor-pointer delete-card"
                onClick={handleRemoveClick}
              >
                Delete
              </div>
              {!isArchived && (
                <div
                  className="px-3 py-1 text-xs hover-bg-gray-100 cursor-pointer archive-card"
                  onClick={handleArchiveClick}
                >
                  Archive
                </div>
              )}
              <div
                className={`px-3 py-1 pin-card text-xs hover-bg-gray-100 cursor-pointer ${
                  isPinned ? "text-blue-600" : ""
                }`}
                onClick={handlePinToggle}
              >
                {isPinned ? "Unpin" : "Pin"}
              </div>
            </div>
          )}
        </div>
      )}
    </Draggable>
  );
};

export default TaskCard;
