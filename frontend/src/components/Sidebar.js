import React from "react";
import { Link } from "react-router-dom";
import logo from "../images/logo/logo-taskhub.png";
const Sidebar = ({ setActiveContent, activeContent }) => {
  return (
    <div
      className="px-4 py-2 text-center border text-white w-40"
      style={{ height: "calc(100vh - 0px)" }}
    >
      <div className="flex items-center  mt-5">
        <img src={logo} alt="logo" className="w-30" />
      </div>
      <ul className="mt-10 space-y-1">
        <li>
          <Link
            className={`block mb-5 rounded-lg text-center px-4 py-2 text-xs font-medium ${
              activeContent === "board"
                ? "bg-gray-400"
                : "text-gray-700 hover:bg-gray-400"
            }`}
            onClick={() => setActiveContent("board")}
          >
            Task Board
          </Link>
        </li>
        <li>
          <Link
            className={`block rounded-lg text-center px-4 py-2 text-xs font-medium ${
              activeContent === "members"
                ? "bg-gray-400"
                : "text-gray-700 hover:bg-gray-400"
            }`}
            onClick={() => setActiveContent("members")}
          >
            Members
          </Link>
        </li>
      </ul>
      {/* Add the remaining bottom part of the sidebar */}
    </div>
  );
};

export default Sidebar;
