import React from 'react';
import logo from '../images/logo/logo-taskhub.png';

function AboutUs() {
    return (
        <div id="about-us-section" className="bg-white h-auto p-4 sm:p-6 md:p-8 lg:p-10 shadow-lg rounded-lg mx-auto w-full sm:w-10/12 md:w-8/12 lg:w-6/12">
            <div className="text-center">
                <img src={logo} alt="Application Logo" className=" sm:w-32 md:w-40 lg:w-1/4 rounded-full mx-auto mb-4" />
                <h1 className="text-2xl text-center  font-semibold text-indigo-600 mb-8 sm:text-2xl sm:text-2xl md:text-4xl lg:text-3xl mb-6 font-semibold">About Our Task Management Application</h1>
            </div>

            <p className="text-gray-700">
                Welcome to our modern task management application! We are committed to delivering a powerful and user-friendly platform to enhance your productivity and organization.
            </p>

            <p className="text-gray-700 mt-2 sm:mt-4">
                Our mission is clear: to empower individuals and teams to efficiently manage tasks and achieve their goals. We understand the demands of today's fast-paced world, and we're here to simplify your task management experience.
            </p>

            <p className="text-gray-700 mt-2 sm:mt-4">
                With our app, you can effortlessly create, organize, and prioritize tasks. Stay in control of your daily responsibilities, projects, and deadlines. Our solution is designed to cater to the diverse needs of users, from students to professionals.
            </p>

            <p className="text-gray-700 mt-2 sm:mt-4">
                Your success is our top priority. We are dedicated to continuous improvement and innovation to ensure that our application remains an invaluable tool on your journey to productivity and achievement.
            </p>

            <p className="text-gray-700 mt-2 sm:mt-4">
                Thank you for choosing our task management application. We are excited to be part of your path to success!
            </p>
        </div>
    );
}

export default AboutUs;
