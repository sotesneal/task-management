import React, { createContext, useContext, useState } from 'react';

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [profilePictureUrl, setProfilePictureUrl] = useState(null);

  const updateUserProfilePicture = (newProfilePictureUrl) => {
    setProfilePictureUrl(newProfilePictureUrl);
  };

  return (
    <UserContext.Provider value={{ profilePictureUrl, updateUserProfilePicture }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUserProfilePicture = () => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUserProfilePicture must be used within a UserProvider');
  }
  return context;
};
