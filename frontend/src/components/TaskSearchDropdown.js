import React, { useState, useEffect } from 'react';
import taskService from '../services/taskService';

const TaskSearchDropdown = ({ searchQuery }) => {
  const [searchResults, setSearchResults] = useState([]);

  useEffect(() => {
    if (searchQuery) {
        const tokenString = localStorage.getItem("token");
        const authToken = JSON.parse(tokenString);
      taskService.searchTasks(authToken, searchQuery)
        .then((results) => {
          setSearchResults(results); // Assuming the response is an array of tasks
        })
        .catch((error) => {
          console.error('Error fetching search results:', error);
        });
    } else {
      // Clear search results if the search query is empty
      setSearchResults([]);
    }
  }, [searchQuery]);
  const getStatusLabel = (status) => {
    switch (status) {
      case 0:
        return 'Todo';
      case 1:
        return 'In Progress';
      case 2:
        return 'Done';
      default:
        return '';
    }
  };

  return (
    <div className="search-dropdown absolute top-12 w-80 bg-white z-10 rounded-md shadow-md">
      {searchResults.map((result) => (
        <div key={result.id} className="search-result p-2 hover:bg-indigo-100 cursor-pointer" >
          <div className="text-sm font-semibold">{result.title}</div>
          <div className="text-xs mt-1 text-gray-500">In list: {getStatusLabel(result.status)}</div>
          
          {/* Add more relevant details here */}
        </div>
      ))}
    </div>
  );
};

export default TaskSearchDropdown;
