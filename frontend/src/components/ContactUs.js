import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../images/logo/logo-taskhub.png'
function ContactUsForm() {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    message: '',
  });

  const [isLoading, setIsLoading] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);

    // Simulate form submission delay
    setTimeout(() => {
      setIsLoading(false);
      // Process the form data, e.g., send it to a server
      console.log(formData);
    }, 2000);
  };

  return (
    <form
      onSubmit={handleSubmit}
      className="mb-0 mt-10 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8 w-[430px] h-auto"
    >
        <img src={logo} alt="logo" className="w-3/4 mx-auto" />
      <p className="text-center  font-semibold text-indigo-600 mb-8 sm:text-2xl">
        Contact Us
      </p>
      <div class="relative">
            <input
              type="text"
              id="emailOrUsername" // Update id and name to emailOrUsername
              name="emailOrUsername"
              aria-describedby="emailOrUsername"
              value={formData.name}
              onChange={handleInputChange}
              className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
                formData.name ? 'border-red-500' : 'border-gray-200'
              } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
              placeholder=" "
            />
            {!formData.name && (
              <label
                for="emailOrUsername"
                class="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
              >
               Name
              </label>
            )}
            {formData.name && (
              <p className="text-red-500 text-xs">{formData.name}</p>
            )}
          </div>
          <div class="relative">
            <input
              type="text"
              id="email" // Update id and name to emailOrUsername
              name="email"
              aria-describedby="email"
              value={formData.email}
              onChange={handleInputChange}
              className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
                formData.email ? 'border-red-500' : 'border-gray-200'
              } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
              placeholder=" "
            />
            {!formData.email && (
              <label
                for="emailOrUsername"
                class="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
              >
              Email
              </label>
            )}
            {formData.email && (
              <p className="text-red-500 text-xs">{formData.email}</p>
            )}
          </div>
          <div class="relative">
            <textarea
              type="text"
              id="message" // Update id and name to emailOrUsername
              name="message"
              aria-describedby="message"
              value={formData.message}
              rows={5}
              onChange={handleInputChange}
              className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
                formData.name ? 'border-red-500' : 'border-gray-200'
              } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
              placeholder=" "
            />
            {!formData.message && (
              <label
                for="emailOrUsername"
                class="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
              >
               Message
              </label>
            )}
            {formData.name && (
              <p className="text-red-500 text-xs">{formData.name}</p>
            )}
          </div>
      <button
        type="submit"
        className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white"
        disabled={isLoading}
      >
        {isLoading ? (
          <span>
            <i className="fas fa-spinner fa-spin"></i> Submitting...
          </span>
        ) : (
          <span>Submit</span>
        )}
      </button>
      <p className="text-center text-sm text-gray-500">
        <Link to="/" className="underline">
          Back to Home
        </Link>
      </p>
    </form>
  );
}

export default ContactUsForm;
