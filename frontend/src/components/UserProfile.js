import React, { useState, useEffect, useRef } from "react";
import { AuthService } from "../services/authService";
import { CameraIcon } from "@heroicons/react/outline";
import { useUserProfilePicture } from "./UserContext";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
const defaultBackgroundColor = "#007BFF";

const UserProfile = () => {
  const [errors, setErrors] = useState({});
  const [, setGeneralError] = useState("");
  const [isEditingFirstName, setIsEditingFirstName] = useState(false);
  const [isEditingLastName, setIsEditingLastName] = useState(false);
  const [profilePicture, setProfilePicture] = useState(null);
  const [userInfo, setUserInfo] = useState(null);
  const [isEditingProfilePicture, setIsEditingProfilePicture] = useState(false); // State for edit mode
  const profilePictureInputRef = useRef(null);
  const [firstNameError, setFirstNameError] = useState("");
  const [lastNameError, setLastNameError] = useState("");
  const [loading, setLoading] = useState(false);
  const [isEditingEmail, setIsEditingEmail] = useState(false);
  const [newEmail, setNewEmail] = useState(userInfo ? userInfo.email : "");
  const navigate = useNavigate();
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [confirmPasswordVisible, setConfirmPasswordVisible] = useState(false);
  const [formData, setFormData] = useState({
    oldPassword: "",
    newPassword: "",
  });
  const toggleEmailEditMode = () => {
    setIsEditingEmail(!isEditingEmail);
  };
  const toggleFirstNameEditMode = () => {
    setIsEditingFirstName(!isEditingFirstName);
  };

  const toggleLastNameEditMode = () => {
    setIsEditingLastName(!isEditingLastName);
  };

  const { updateUserProfilePicture } = useUserProfilePicture();
  // Validation functions for first name and last name
  const validateFirstName = (firstName) => {
    if (firstName.length < 2 || firstName.length > 100) {
      return "First Name must be between 2 and 100 characters.";
    }
    return "";
  };

  const validateLastName = (lastName) => {
    if (lastName.length < 2 || lastName.length > 100) {
      return "Last Name must be between 2 and 100 characters.";
    }
    return "";
  };
  // Password validation errors state
  const [passwordValidationErrors, setPasswordValidationErrors] = useState([]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));

    if (name === "newPassword") {
      const passwordPattern =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&*!])[A-Za-z\d@#$%^&*!]{8,}$/;
      if (!passwordPattern.test(value)) {
        setErrors((prevErrors) => ({
          ...prevErrors,
          [name]:
            "Password must meet the specified criteria. Please check and try again.",
        }));
      } else {
        setErrors((prevErrors) => ({
          ...prevErrors,
          [name]: "", // Clear the error if the password format is valid
        }));
      }
    } else if (name === "oldPassword") {
      // Update the error message for the "oldPassword" field here
      setErrors((prevErrors) => ({
        ...prevErrors,
        [name]: "Please enter your current password.",
      }));
    } else if (name === "confirmNewPassword") {
      // Clear the error message for the "confirmNewPassword" field when the user starts typing
      setErrors((prevErrors) => ({
        ...prevErrors,
        [name]: "",
      }));
    }
  };

  const handleChangePassword = async (e) => {
    e.preventDefault();

    // Disable the submit button while processing
    setLoading(true);

    const validationErrors = validateForm();
    if (Object.keys(validationErrors).length === 0) {
      try {
        // Simulate a delay using setTimeout
        setTimeout(async () => {
          const token = localStorage.getItem("token");
          try {
            const response = await AuthService.changePassword(
              JSON.parse(token),
              formData
            );

            // Check if the password change was successful
            if (response === "Password changed successfully.") {
              toast.success("Password changed successfully.");
              setPasswordValidationErrors({}); // Clear password validation errors
              setGeneralError(""); // Clear any previous general error
            } else {
              // Handle specific error messages from the backend response
              if (typeof response === "string") {
                setGeneralError(response);
              } else if (Array.isArray(response)) {
                // Check for specific error message for incorrect current password
                if (response.includes("Incorrect password.")) {
                  setErrors({ oldPassword: "Incorrect current password." });
                } else {
                  // Handle other error messages
                  setGeneralError(response.join("\n")); // Join the array of error messages
                }
              }
            }
          } catch (error) {
            setGeneralError("An error occurred while changing password");
          }

          // Re-enable the submit button after processing
          setLoading(false);
        }, 1000); // Delay of 1 second (1000 milliseconds)
      } catch (error) {
        setGeneralError("An error occurred while changing password");
        // Re-enable the submit button after processing
        setLoading(false);
      }
    } else {
      setErrors(validationErrors);
      // Re-enable the submit button after processing
      setLoading(false);
    }
  };

  const validateForm = () => {
    const validationErrors = {};

    // Confirm password validation
    if (formData.newPassword !== formData.confirmNewPassword) {
      validationErrors.confirmNewPassword = "Passwords do not match.";
    }
    // Password is required
    if (!formData.newPassword) {
      validationErrors.newPassword = "New Password is required.";
    }
    if (!formData.oldPassword) {
      validationErrors.oldPassword = "Old Password is required.";
    }
    if (!formData.confirmNewPassword) {
      validationErrors.confirmNewPassword = "Confirm Password is required.";
    }
    // Password validation
    const passwordPattern =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (!passwordPattern.test(formData.newPassword)) {
      // Use a space in the field name for the error message
      validationErrors.newPassword = [
        "Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character.",
      ];
    }

    // Update passwordValidationErrors state
    setPasswordValidationErrors(validationErrors);

    return validationErrors;
  };

  useEffect(() => {
    fetchUserInfo();
  }, []);

  const fetchUserInfo = async () => {
    try {
      const token = localStorage.getItem("token");
      const user = await AuthService.getUserInfo(JSON.parse(token));
      setUserInfo(user);
    } catch (error) {
      console.error("Error fetching user info:", error);
    }
  };

  // Function to handle clicking on the profile picture
  const handleClickProfilePicture = () => {
    // Trigger the file input when the profile picture is clicked
    if (profilePictureInputRef.current) {
      profilePictureInputRef.current.click();
    }
  };
  const handleProfilePictureUpload = (event) => {
    const file = event.target.files[0];
    if (file) {
      setProfilePicture(file);
      setIsEditingProfilePicture(true); // Show edit mode when a file is selected
    }
  };

  // Function to handle profile picture upload
  const handleSaveProfilePicture = async () => {
    try {
      const tokenString = localStorage.getItem("token");
      const authToken = JSON.parse(tokenString);
      const newProfilePictureUrl = await AuthService.uploadProfilePicture(
        authToken,
        profilePicture
      );

      // Update the user's profile picture URL in the state and context
      setUserInfo((prevUserInfo) => ({
        ...prevUserInfo,
        profilePictureUrl: newProfilePictureUrl,
      }));
      updateUserProfilePicture(newProfilePictureUrl);

      // Exit edit mode and clear the profilePicture state
      setIsEditingProfilePicture(false);
      setProfilePicture(null);

      // Delay the page reload to allow the user to see the toast
      setTimeout(() => {
        // Reload the page to reflect the new profile picture
        window.location.reload();
      }, 1000); // Adjust the delay time as needed
      // Show a success toast notification
      toast.success("Profile picture uploaded successfully!");
    } catch (error) {
      // Handle the error if the upload fails
      console.error("Error uploading profile picture:", error);
      // Show an error toast notification
      toast.error("Error uploading profile picture. Please try again.");
    }
  };

  const handlePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };
  const handleConfirmPasswordVisibility = () => {
    setConfirmPasswordVisible(!confirmPasswordVisible);
  };
  const renderInput = (name, label) => (
    <div className={`relative ${errors[name] ? "mb-2" : "mb-6"}`}>
      <input
        type={
          name === "oldPassword" ||
          name === "newPassword" ||
          name === "confirmNewPassword"
            ? passwordVisible
              ? "text"
              : "password"
            : "text"
        } // Set the correct input type
        id={name}
        name={name}
        onChange={handleInputChange}
        className={`block px-2.5 pb-2.5 pt-4 w-full text-sm text-black bg-transparent rounded-lg border-2 ${
          errors[name] ? "border-red-500" : "border-gray-200"
        } p-4 pe-12 text-sm shadow-sm appearance-none dark:text-white dark:border-indigo-300 dark:focus:border-indigo-500 focus:border-indigo-600 focus:ring-0 focus:border-gray-200 peer`}
        placeholder=" "
      />
      {!errors[name] && (
        <label
          htmlFor={name}
          className="absolute text-sm text-gray-500 dark:text-gray-500 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-500 px-2  peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1"
        >
          {label}
        </label>
      )}
      {errors[name] && <p className="text-red-500 text-xs">{errors[name]}</p>}
      {(name === "oldPassword" || name === "confirmNewPassword") && (
        <button
          type="button"
          className="absolute right-3 top-4 text-gray-500"
          onClick={
            name === "oldPassword"
              ? handlePasswordVisibility
              : handleConfirmPasswordVisibility
          }
        >
          {name === "oldPassword" ? (
            passwordVisible ? (
              <i className="fas fa-eye-slash"></i>
            ) : (
              <i className="fas fa-eye"></i>
            )
          ) : null}
        </button>
      )}
    </div>
  );
  const goBackToBoard = () => {
    navigate("/user-dashboard");
  };
  // Function to cancel profile picture edit
  const handleCancelProfilePictureEdit = () => {
    // Clear the selected file and exit edit mode
    setProfilePicture(null);
    setIsEditingProfilePicture(false);
  };

  // Function to get capitalized user's initials
  function getCapitalizedUserInitials(user) {
    if (user && user.firstname && user.lastname) {
      const initials =
        user.firstname[0].toUpperCase() + user.lastname[0].toUpperCase();
      return initials;
    }
    return "";
  }
  const updateEmail = async (newEmail) => {
    try {
      const token = localStorage.getItem("token");
      const response = await AuthService.updateEmail(
        JSON.parse(token),
        newEmail
      );

      // Check if the update was successful
      if (response === "Email updated successfully.") {
        // Update the user's Email in the userInfo state
        setUserInfo((prevUserInfo) => ({
          ...prevUserInfo,
          email: newEmail,
        }));
        toast.success("Email updated successfully.");
      } else {
        // Handle any error messages from the backend response
        toast.error(response);
      }
    } catch (error) {
      console.error("Error updating Email:", error);
    }
  };
  const [newFirstName, setNewFirstName] = useState(
    userInfo ? userInfo.firstname : ""
  );
  const [newLastName, setNewLastName] = useState(
    userInfo ? userInfo.lastname : ""
  );
  const updateLastName = async (newLastname) => {
    try {
      const token = localStorage.getItem("token");
      const response = await AuthService.updateLastname(
        JSON.parse(token),
        newLastname
      );

      // Check if the update was successful
      if (response === "Lastname updated successfully.") {
        // Update the user's Lastname in the userInfo state
        setUserInfo((prevUserInfo) => ({
          ...prevUserInfo,
          lastname: newLastname,
        }));
        toast.success("Lastname updated successfully.");
      } else {
        // Handle any error messages from the backend response
        toast.error(response);
      }
    } catch (error) {
      console.error("Error updating Lastname:", error);
    }
  };
  const updateFirstName = async (newFirstName) => {
    const validationError = validateFirstName(newFirstName);
    if (validationError) {
      setFirstNameError(validationError);
      return; // Do not proceed if there's a validation error
    }

    try {
      const token = localStorage.getItem("token");
      const response = await AuthService.updateFirstname(
        JSON.parse(token),
        newFirstName
      );

      // Check if the update was successful
      if (response === "Firstname updated successfully.") {
        // Update the user's Lastname in the userInfo state
        setUserInfo((prevUserInfo) => ({
          ...prevUserInfo,
          firstname: newFirstName,
        }));
        toast.success("Firstname updated successfully.");
      } else {
        // Handle any error messages from the backend response
        toast.error(response);
      }
    } catch (error) {
      console.error("Error updating Firstname:", error);
    }
  };

  // Function to handle change in the first name input
  const handleFirstNameChange = (e) => {
    const newFirstName = e.target.value;
    setNewFirstName(newFirstName);
    const validationError = validateFirstName(newFirstName);
    setFirstNameError(validationError);
  };
  // Function to handle change in the last name input
  const handleLastNameChange = (e) => {
    const newLastName = e.target.value;
    setNewLastName(newLastName);
    const validationError = validateLastName(newLastName);
    setLastNameError(validationError);
  };
  return (
    <div className="container mx-auto  flex flex-col items-center">
      {/* User Profile */}
      <h2 className="text-2xl text-center font-semibold">User Profile</h2>
      <button
        onClick={goBackToBoard} // Define a function to handle going back to the board
        className="absolute top-0 left-0 mt-2 ml-2 bg-gray-400 text-gray-800 rounded-md px-3 py-2 text-sm"
      >
        Go Back to Board
      </button>
      <div className="mb-8 mt-3">
        <div
          className="w-40 h-40 rounded-full flex items-center justify-center cursor-pointer relative"
          style={{
            backgroundColor: defaultBackgroundColor,
            color: "white",
          }}
          onClick={handleClickProfilePicture}
        >
          {profilePicture ? (
            <img
              src={URL.createObjectURL(profilePicture)}
              alt="Profile"
              className="w-40 h-40 rounded-full"
            />
          ) : userInfo && userInfo.profilePictureUrl ? (
            <img
              src={`http://localhost:5228/${userInfo.profilePictureUrl}`}
              alt="Profile"
              className="w-40 h-40 rounded-full"
            />
          ) : (
            getCapitalizedUserInitials(userInfo)
          )}
          {/* Camera icon */}
          {isEditingProfilePicture && (
            <div className="absolute inset-0 flex items-center justify-center opacity-0 hover:opacity-100">
              <CameraIcon
                className="h-10 w-10 text-white bg-indigo-600 rounded-full p-1"
                onClick={(e) => {
                  e.stopPropagation();
                }}
              />
            </div>
          )}
        </div>
        {/* Hidden file input for profile picture upload */}
        <input
          type="file"
          accept="image/*"
          ref={profilePictureInputRef}
          className="hidden"
          onChange={handleProfilePictureUpload}
        />
        {isEditingProfilePicture && (
          <div className="mt-2 text-center">
            <button
              className="px-4 py-2 bg-indigo-600 text-white rounded-md hover:bg-indigo-700 mr-2"
              onClick={handleSaveProfilePicture}
            >
              Save
            </button>
            <button
              className="px-4 py-2 bg-gray-400 text-gray-800 rounded-md hover:bg-gray-500"
              onClick={handleCancelProfilePictureEdit}
            >
              Cancel
            </button>
          </div>
        )}
      </div>

      {/* User Information and Change Password (side by side) */}
      <div className="flex flex-row mb-4 w-6/12">
        {/* User Information */}
        <div className="flex-1 mr-4">
          <div className="bg-white rounded p-4 shadow-md w-full">
            <h3 className="text-xl font-semibold mb-4">User Information</h3>
            <div className="mb-3">
              <label className="block text-sm font-medium text-gray-700">
                Username
              </label>
              <p className="text-gray-900">{userInfo && userInfo.username}</p>
            </div>
            {isEditingFirstName ? (
              <div className="mb-2">
                <label className="block text-sm font-medium text-gray-700">
                  First Name
                </label>
                <input
                  type="text"
                  className="block w-full px-2 py-1 rounded-md border-2 border-gray-300 text-sm"
                  placeholder="New First Name"
                  value={newFirstName}
                  onChange={handleFirstNameChange}
                />
                {firstNameError && (
                  <p className="text-red-500 text-xs">{firstNameError}</p>
                )}
                <div className="mt-1">
                  <button
                    onClick={() => {
                      // Call a function to update the first name when saving
                      updateFirstName(newFirstName);
                      setIsEditingFirstName(false); // Disable edit mode
                    }}
                    className="bg-indigo-600 text-white rounded-md px-2 py-1 mr-2 text-sm"
                  >
                    Save
                  </button>
                  <button
                    onClick={() => {
                      setNewFirstName(userInfo.firstname); // Reset the first name value
                      setIsEditingFirstName(false); // Disable edit mode
                    }}
                    className="bg-gray-400 text-gray-800 rounded-md px-2 py-1 text-sm"
                  >
                    Cancel
                  </button>
                </div>
              </div>
            ) : (
              <div className="mb-2">
                <label className="block text-sm font-medium text-gray-700">
                  First Name
                </label>
                <div className="flex items-center">
                  {" "}
                  {/* Wrap the button in a div and apply flex */}
                  <p className="text-gray-900">
                    {userInfo && userInfo.firstname}
                  </p>
                  <button
                    onClick={toggleFirstNameEditMode}
                    className="ml-2 text-indigo-600 text-xs hover:text-indigo-800 underline cursor-pointer"
                  >
                    Edit
                  </button>
                </div>
              </div>
            )}

            {isEditingLastName ? (
              <div className="mb-2">
                <label className="block text-sm font-medium text-gray-700">
                  Last Name
                </label>
                <input
                  type="text"
                  className="block w-full px-2 py-1 rounded-md border-2 border-gray-300 text-sm" // Decreased padding and font size
                  placeholder="New Last Name"
                  value={newLastName}
                  onChange={handleLastNameChange}
                />
                {lastNameError && (
                  <p className="text-red-500 text-xs">{lastNameError}</p>
                )}
                <div className="mt-1">
                  <button
                    onClick={() => {
                      // Call a function to update the first name when saving
                      updateLastName(newLastName);

                      setIsEditingLastName(false); // Disable edit mode
                    }}
                    className="bg-indigo-600 text-white rounded-md px-2 py-1 mr-2 text-sm"
                  >
                    Save
                  </button>
                  <button
                    onClick={() => {
                      setNewLastName(userInfo.lastname); // Reset the first name value
                      setIsEditingLastName(false); // Disable edit mode
                    }}
                    className="bg-gray-400 text-gray-800 rounded-md px-2 py-1 text-sm"
                  >
                    Cancel
                  </button>
                </div>
              </div>
            ) : (
              <div className="mb-2">
                <label className="block text-sm font-medium text-gray-700">
                  Last Name
                </label>
                <div className="flex items-center">
                  <p className="text-gray-900">
                    {userInfo && userInfo.lastname}
                  </p>
                  <button
                    onClick={toggleLastNameEditMode}
                    className="ml-2 text-indigo-600 text-xs hover:text-indigo-800 underline cursor-pointer"
                  >
                    Edit
                  </button>
                </div>
              </div>
            )}
            {isEditingEmail ? (
              <div className="mb-2">
                <label className="block text-sm font-medium text-gray-700">
                  Email
                </label>
                <input
                  type="email"
                  className="block w-full px-2 py-1 rounded-md border-2 border-gray-300 text-sm" // Decreased padding and font size
                  placeholder="New Email"
                  value={newEmail}
                  onChange={(e) => setNewEmail(e.target.value)}
                />

                <div className="mt-1">
                  <button
                    onClick={() => {
                      // Call the updateEmail function when saving
                      updateEmail(newEmail);
                      setIsEditingEmail(false); // Disable edit mode
                    }}
                    className="bg-indigo-600 text-white rounded-md px-2 py-1 mr-2 text-sm"
                  >
                    Save
                  </button>
                  <button
                    onClick={() => {
                      setNewEmail(userInfo.email); // Reset the email value
                      setIsEditingEmail(false); // Disable edit mode
                    }}
                    className="bg-gray-400 text-gray-800 rounded-md px-2 py-1 text-sm"
                  >
                    Cancel
                  </button>
                </div>
              </div>
            ) : (
              <div>
                <label className="block text-sm font-medium text-gray-700">
                  Email
                </label>
                <div className="flex items-center">
                  <p className="text-gray-900">{userInfo && userInfo.email}</p>
                  <button
                    onClick={toggleEmailEditMode}
                    className="ml-2 text-indigo-600 hover:text-indigo-800 text-xs underline cursor-pointer"
                  >
                    Edit
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>

        {/* Change Password Form */}
        <div className="flex-1">
          <div className="bg-white rounded p-4 shadow-md">
            <h3 className="text-xl font-semibold mb-4">Change Password</h3>
            {errors.generalError && (
              <p className="text-red-500 text-sm text-center mb-4">
                {errors.generalError}
              </p>
            )}
            {passwordValidationErrors.length > 0 && (
              <div className="mb-4">
                {passwordValidationErrors.map((error, index) => (
                  <p key={index} className="text-red-500 text-sm">
                    {error}
                  </p>
                ))}
              </div>
            )}
            <form onSubmit={handleChangePassword}>
              {renderInput("oldPassword", "Old Password")}
              {renderInput("newPassword", "New Password")}
              {renderInput("confirmNewPassword", "Confirm Password")}
              <button
                type="submit"
                className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white relative"
              >
                {loading ? (
                  <span>
                    <i className="fas fa-spinner fa-spin"></i> Changing
                    Password....
                  </span>
                ) : (
                  <span>Change Password</span>
                )}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
