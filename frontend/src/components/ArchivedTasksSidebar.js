import React, { useState, useEffect } from "react";
import taskService from "../services/taskService";
import { toast } from "react-toastify";
import { ArchiveIcon, ChevronLeftIcon } from "@heroicons/react/solid";

const ArchivedTasksSidebar = ({
  token,
  isOpen,
  onClose,
  updateTaskOnUnarchive,
}) => {
  const [archivedTasks, setArchivedTasks] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  useEffect(() => {
    if (isOpen) {
      // Fetch archived tasks when the sidebar is opened
      async function fetchArchivedTasks() {
        try {
          const response = await taskService.getArchivedTasks(token);
          // Initialize isArchived property based on local storage
          const tasksWithArchivedStatus = response.map((task) => ({
            ...task,
            isArchived: localStorage.getItem(`archived_${task.id}`) === "true",
          }));
          setArchivedTasks(tasksWithArchivedStatus);
        } catch (error) {
          console.error("Error fetching archived tasks:", error);
        }
      }

      fetchArchivedTasks();
    }
  }, [isOpen, token, updateTaskOnUnarchive]);

  const sendToBoard = async (taskId) => {
    await taskService
      .unarchiveTask(token, taskId)
      .then((response) => {
        // Handle the success response here
        console.log("Task unarchived:", response);

        // Find the task with the specified taskId in the archivedTasks array
        const taskToUpdate = archivedTasks.find((task) => task.id === taskId);

        if (taskToUpdate) {
          taskToUpdate.isArchived = false;
          localStorage.setItem(`archived_${taskId}`, "false");

          // Call the updateTaskOnUnarchive function with the taskId
          updateTaskOnUnarchive(taskId);

          // Update the isArchived property of the specific task

          // Update the state to trigger a re-render
          setArchivedTasks((prevArchivedTasks) =>
            prevArchivedTasks.filter((task) => task.id !== taskId)
          );
          toast.success("Task successfully unarchived!", { autoClose: 1000 });
        }
      })
      .catch((error) => {
        console.error("Error unarchiving task:", error);
      });
  };

  const deleteTask = async (taskId) => {
    try {
      const confirmToastId = toast.info(
        <div className="bg-white p-2 rounded-lg">
          <div className="text-lg font-semibold mb-2">Delete Archived Task</div>
          <div className="text-gray-700 text-sm mb-4">
            Are you sure you want to delete this archived task?
          </div>
          <div className="flex justify-center">
            <button
              className="bg-red-500 text-white font-semibold px-4 py-2 rounded-lg mr-2 hover:bg-red-600"
              onClick={async () => {
                toast.dismiss(confirmToastId);
                const tokenString = localStorage.getItem("token");
                const authToken = JSON.parse(tokenString);
                await taskService.removeTask(authToken, taskId);
                toast.success("Archived Task deleted!", {
                  autoClose: 2000,
                });
              }}
            >
              OK
            </button>
            <button
              className="bg-gray-300 text-gray-700 font-semibold px-4 py-2 rounded-lg hover:bg-gray-400"
              onClick={() => {
                toast.dismiss(confirmToastId);
              }}
            >
              Cancel
            </button>
          </div>
        </div>,
        {
          autoClose: false,
          position: toast.POSITION.TOP_CENTER,
          closeButton: false,
        }
      );
    } catch (error) {
      console.error("Error removing task:", error);
      toast.error("Error deleting the card.", { autoClose: 2000 });
    }
  };
  const searchArchivedTasks = async () => {
    try {
      const results = await taskService.searchArchivedTasks(token, searchQuery);
      setArchivedTasks(results);
    } catch (error) {
      console.error("Error searching archived tasks:", error);
    }
  };

  // Handle input change and trigger search
  const handleInputChange = (e) => {
    setSearchQuery(e.target.value);
    searchArchivedTasks();
  };
  return (
    <div className={`sidebar ${isOpen ? "open" : ""}`}>
      <div className="bg-blue-500 text-white p-2  text-center flex items-center">
        <button onClick={onClose} className="arrow-button">
          <ChevronLeftIcon className="w-6 h-6 " />
        </button>
        <h2 className="text-2xl ml-10 font-bold">Archived Tasks</h2>
      </div>

      <div className="sidebar-content">
        <div className="search-bar p-2">
          <input
            type="text"
            placeholder="Search archived tasks..."
            className="px-3 py-2 rounded-md w-full border focus:outline-none focus:ring-2 focus:ring-indigo-400"
            value={searchQuery}
            onChange={handleInputChange} // Trigger search on input change
          />
        </div>
        <div className="task-list mt-1 space-y-2 overflow-y-auto">
  {archivedTasks.length === 0 && searchQuery === "" && (
    <div className="text-gray-500 text-center mt-4">
      No archived tasks yet
    </div>
  )}
  {archivedTasks.length === 0 && searchQuery !== "" && (
    <div className="text-gray-500 text-center mt-4">
      No archived tasks found for your search.
    </div>
  )}
  {archivedTasks.length > 0 && (
    archivedTasks.map((task) => (
      <div
        key={task.id}
        className="bg-white rounded-lg p-4 border border-gray-300 shadow-md rounded-md relative"
      >
        <p className="text-lg font-semibold">
          {task.title}
          <span className="text-xs text-gray-500 block">
            <ArchiveIcon className="w-4 h-4 inline-block mr-1" />
            Archived
          </span>
        </p>
        <div className="mt-1">
          <button
            className="text-blue-500 text-xs hover:underline cursor-pointer"
            onClick={() => sendToBoard(task.id)}
          >
            Send to board
          </button>
          <button
            className="text-red-500 text-xs hover:underline cursor-pointer ml-2"
            onClick={() => deleteTask(task.id)}
          >
            Delete
          </button>
        </div>
      </div>
    ))
  )}
</div>

      </div>
    </div>
  );
};

export default ArchivedTasksSidebar;
