import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import AdminDashboardPage from '../src/admin/pages/AdminDashboardPage';
import LoginPage from './pages/LoginPage';
import '@fortawesome/fontawesome-free/css/all.min.css';
import RegisterPage from './pages/RegisterPage';
import UserDashboardPage from './pages/UserDashboardPage';
import ForgotPasswordForm from './pages/ForgotPasswordPage';
import ResetPasswordForm from './pages/ResetPasswordPage';
import AboutUsPage from './pages/AboutUsPage';
import UserProfile from './components/UserProfile';
import { UserProvider } from './components/UserContext';

import './App.css';

function App() {
  return (
    <BrowserRouter>
      <ToastContainer />
      <UserProvider> {/* Wrap your app with UserProvider */}
        <Routes>
          {/* ... Other routes */}
          <Route path="/admin-dashboard" element={<AdminDashboardPage />} />
          <Route path="/" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/user-dashboard/*" element={<UserDashboardPage />} />
          <Route path="/forgot-password" element={<ForgotPasswordForm />} />
          <Route path="/reset-password/:userId/:code" element={<ResetPasswordForm/>} />
          <Route path="/about" element={<AboutUsPage />} />
          <Route path='/profile' element={<UserProfile />} />
        </Routes>
      </UserProvider>
    </BrowserRouter>
  );
}

export default App;
